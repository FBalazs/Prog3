import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class CaesarFrame extends JFrame {
    public static void main(String[] args) {
        new CaesarFrame().setVisible(true);
    }
    
    private DefaultComboBoxModel<Character> comboBoxModelOffset;
    private JComboBox<Character> comboBoxOffset;
    private JTextField tfIn, tfOut;
    private JLabel lblOutput;
    private JButton btnCode;
    
    private boolean decode = false;
    
    public CaesarFrame() {
        super("SwingLab"); //setTitle("SwingLab");
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 110);
        setResizable(true);
        
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        getContentPane().setLayout(gridBagLayout);
        
        comboBoxModelOffset = new DefaultComboBoxModel<>();
        for(char c = 'A'; c <= 'Z'; ++c)
            comboBoxModelOffset.addElement(c);
        comboBoxOffset = new JComboBox<>(comboBoxModelOffset);
        GridBagConstraints gbc_comboBox = new GridBagConstraints();
        gbc_comboBox.insets = new Insets(0, 0, 5, 5);
        gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_comboBox.gridx = 1;
        gbc_comboBox.gridy = 1;
        getContentPane().add(comboBoxOffset, gbc_comboBox);
        
        tfIn = new JTextField(20);
        GridBagConstraints gbc_tfIn = new GridBagConstraints();
        gbc_tfIn.insets = new Insets(0, 0, 5, 5);
        gbc_tfIn.fill = GridBagConstraints.HORIZONTAL;
        gbc_tfIn.gridx = 2;
        gbc_tfIn.gridy = 1;
        getContentPane().add(tfIn, gbc_tfIn);
        
        btnCode = new JButton("Code");
        GridBagConstraints gbc_btnCode = new GridBagConstraints();
        gbc_btnCode.insets = new Insets(0, 0, 5, 5);
        gbc_btnCode.gridx = 3;
        gbc_btnCode.gridy = 1;
        getContentPane().add(btnCode, gbc_btnCode);
        
        lblOutput = new JLabel("Output:");
        GridBagConstraints gbc_lblOutput = new GridBagConstraints();
        gbc_lblOutput.anchor = GridBagConstraints.EAST;
        gbc_lblOutput.insets = new Insets(0, 0, 5, 5);
        gbc_lblOutput.gridx = 1;
        gbc_lblOutput.gridy = 2;
        getContentPane().add(lblOutput, gbc_lblOutput);
        
        tfOut = new JTextField(20);
        GridBagConstraints gbc_tfOut = new GridBagConstraints();
        gbc_tfOut.insets = new Insets(0, 0, 5, 5);
        gbc_tfOut.fill = GridBagConstraints.HORIZONTAL;
        gbc_tfOut.gridx = 2;
        gbc_tfOut.gridy = 2;
        getContentPane().add(tfOut, gbc_tfOut);
        
        btnCode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(decode)
                    updateDecode();
                else
                    updateCode();
            }
        });
        
        tfIn.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                updateCode();
            }
            
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateCode();
            }
            
            @Override
            public void changedUpdate(DocumentEvent e) {
                updateCode();
            }
        });
        

        tfIn.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                decode = false;
            }
        });

        tfOut.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                decode = true;
            }
        });
    }
    
    private void updateCode() {
        tfOut.setText(caesarCode(tfIn.getText(), (Character)comboBoxOffset.getSelectedItem()));
    }
    
    private void updateDecode() {
        tfIn.setText(caesarDecode(tfOut.getText(), (Character)comboBoxOffset.getSelectedItem()));
    }
    
    private String caesarCode(String input, char offset) {
        char[] chars = input.toUpperCase().toCharArray();
        for(int i = 0; i < chars.length; ++i)
            chars[i] = (char)((chars[i]-'A'+offset-'A')%('Z'-'A'+1) + 'A');
        return new String(chars);
    }
    
    private String caesarDecode(String input, char offset) {
        char[] chars = input.toUpperCase().toCharArray();
        for(int i = 0; i < chars.length; ++i)
            chars[i] = (char)((chars[i]-'A'+'Z'-'A'+1-(offset-'A'))%('Z'-'A'+1) + 'A');
        return new String(chars);
    }
}
