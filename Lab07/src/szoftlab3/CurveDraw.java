package szoftlab3;

import java.awt.Graphics;
import java.awt.event.MouseEvent;

public class CurveDraw extends Draw {
    @Override
    public void mouseDragged(MouseEvent arg0) {
        realDraw(arg0, canvas.getBottom());
    }
    
	@Override
    public void makeDraw(Graphics g) {
		g.drawLine(startx, starty, endx, endy);
		startx = endx; starty = endy;
	}
}
