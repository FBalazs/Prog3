package szoftlab3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JColorChooser;

public class ColorChanger implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        Draw.color = JColorChooser.showDialog(null, "Select drawing color!", Draw.color);
    }
}
