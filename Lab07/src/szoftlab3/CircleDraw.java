package szoftlab3;

import java.awt.Graphics;

public class CircleDraw extends Draw {
	@Override
    public void makeDraw(Graphics g) {
	    int r = dist(startx, starty, endx, endy);
	    g.drawOval(startx-r, starty-r, r*2, r*2);
	}
}
