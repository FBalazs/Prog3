import java.io.Serializable;

public class Beer implements Serializable, Comparable<String> {
    String name, style;
    double strength;

    public Beer(String name, String style, double strength) {
        this.name = name;
        this.style = style;
        this.strength = strength;
    }

    @Override
    public int compareTo(String str) {
        return name.compareTo(str);
    }
}
