import java.io.File;
import java.util.List;

public interface Command {
    public File execute(File wd, List<String> args) throws Exception;

    public String help();

    public String name();
}
