import java.util.ArrayList;
import java.util.Iterator;

public class PQueue<T extends Comparable<T>> implements Iterable<T> {
    private ArrayList<T> data = new ArrayList<>();
    
    public void push(T t) {
        int i = 0;
        while(i < data.size() && t.compareTo(data.get(i)) < 0)
            ++i;
        data.add(i, t);
    }
    
    public T pop() throws EmptyQueueException {
        if(data.isEmpty())
            throw new EmptyQueueException();
        return data.remove(0);
    }
    
    public T top() throws EmptyQueueException {
        if(data.isEmpty())
            throw new EmptyQueueException();
        return data.get(0);
    }
    
    public int size() {
        return data.size();
    }
    
    public void clear() {
        data.clear();
    }

    @Override
    public Iterator<T> iterator() {
        return data.iterator();
    }
}
