
public class Test {
    public static void main(String[] args) {
        PQueue<Integer> s = new PQueue<>();
        s.push(1); s.push(2); s.push(3); s.push(4);
        
        for (Integer i : s) {
            System.out.println(i);
        }
        
        try {
            System.out.println(s.top());
            while(s.size() > 1)
                System.out.println(s.pop());
            System.out.println(s.top());
            s.clear();
            System.out.println(s.top());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
