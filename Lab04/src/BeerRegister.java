import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;

public class BeerRegister {
    static HashMap<String, Command> commands = new HashMap<>();

    static void addCommand(Command cmd) {
        commands.put(cmd.name(), cmd);
    }

    static {
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                if(1 < args.size()) {
                    Command cmd = commands.get(args.get(1));
                    if(cmd == null) {
                        System.err.print("Command \"");
                        System.err.print(args.get(1));
                        System.err.println("\" not found!");
                    } else {
                        System.out.println(cmd.help());
                    }
                } else {
                    for(String str : commands.keySet())
                        System.out.println(str);
                }
                return wd;
            }

            @Override
            public String help() {
                return "help [<cmd>]\nIf <cmd> is present prints the usage of <cmd> otherwise lists all the available commands.";
            }

            @Override
            public String name() {
                return "help";
            }
        });

        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                System.exit(0);
                return wd;
            }

            @Override
            public String help() {
                return "exit\nCloses the application.";
            }

            @Override
            public String name() {
                return "exit";
            }
        });

        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws Exception {
                beers.add(new Beer(args.get(1), args.get(2), Double.parseDouble(args.get(3))));
                return wd;
            }

            @Override
            public String help() {
                return "add <name> <style> <stength>";
            }

            @Override
            public String name() {
                return "add";
            }
        });

        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws Exception {
                if(1 < args.size()) {
                    switch(args.get(1)) {
                    case "name":
                        Collections.sort(beers, new NameComparator());
                        break;
                    case "style":
                        Collections.sort(beers, new StyleComparator());
                        break;
                    case "strength":
                        Collections.sort(beers, new StrengthComparator());
                        break;
                    }
                }

                for(Beer beer : beers) {
                    System.out.print(beer.name);
                    System.out.print(" ");
                    System.out.print(beer.style);
                    System.out.print(" ");
                    System.out.print(beer.strength);
                    System.out.println();
                }

                return wd;
            }

            @Override
            public String help() {
                return "list [field]";
            }

            @Override
            public String name() {
                return "list";
            }
        });

        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws Exception {
//                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(wd, args.get(1))));
//                beers = (List<Beer>)ois.readObject();
//                ois.close();
                
                StringBuilder sb = new StringBuilder();
                FileReader fr = new FileReader(new File(wd, args.get(1)));
                int i;
                while((i = fr.read()) != -1)
                    sb.append((char)i);
                fr.close();
                beers = parseJson(sb.toString());
                
                return wd;
            }

            @Override
            public String help() {
                return "load <file>";
            }

            @Override
            public String name() {
                return "load";
            }
        });

        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws Exception {
//                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(wd, args.get(1))));
//                oos.writeObject(beers);
//                oos.close();
                
                FileWriter fw = new FileWriter(new File(wd, args.get(1)));
                fw.write(createJson(beers));
                fw.flush();
                fw.close();
                
                return wd;
            }

            @Override
            public String help() {
                return "save <file>";
            }

            @Override
            public String name() {
                return "save";
            }
        });

        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws Exception {
                int i = Collections.binarySearch(beers, args.get(1));
                if(0 <= i)
                    beers.remove(i);
                return wd;
            }

            @Override
            public String help() {
                return "delete <name>";
            }

            @Override
            public String name() {
                return "delete";
            }
        });
    }

    static String createJson(List<Beer> beers) {
        JsonObjectBuilder job = Json.createObjectBuilder();
        job.add("size", beers.size());

        JsonArrayBuilder jab = Json.createArrayBuilder();
        for(Beer beer : beers) {
            jab.add(Json.createObjectBuilder().add("name", beer.name).add("style", beer.style).add("strength",
                    beer.strength));
        }
        job.add("beers", jab.build());

        return job.build().toString();
    }

    static List<Beer> parseJson(String s) {
        System.out.println("parsing: "+s);
        try {
            ArrayList<Beer> list = new ArrayList<>();

            JsonReader jr = Json.createReader(new StringReader(s));
            JsonArray jarray = jr.readObject().getJsonArray("beers");
            JsonObject jo;
            for(int i = 0; i < jarray.size(); ++i) {
                jo = jarray.getJsonObject(i);
                list.add(new Beer(jo.getString("name"), jo.getString("style"), jo.getJsonNumber("strength").doubleValue()));
            }
            jr.close();

            return list;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static Reader reader;
    static Writer writer;
    static File   wd;

    static List<Beer> beers = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        reader = new InputStreamReader(System.in);
        writer = new OutputStreamWriter(System.out);

        wd = new File(System.getProperty("user.dir"));
        // wd = new File("");

        StringBuilder sb = new StringBuilder();
        ArrayList<String> cmdargs = new ArrayList<>();
        char c;

        boolean flag;
        boolean inquotes;

        while(true) {
            flag = true;
            inquotes = false;

            while(flag) {
                c = (char)reader.read();

                if(c == '\n' || c == '\r') {
                    if(sb.length() != 0) {
                        cmdargs.add(sb.toString());
                        sb.setLength(0);
                        flag = false;
                    }
                } else if(!inquotes && c == ' ') {
                    cmdargs.add(sb.toString());
                    sb.setLength(0);
                } else if(c == '\"') {
                    inquotes = !inquotes;
                } else {
                    sb.append(c);
                }
            }
            Command cmd = commands.get(cmdargs.get(0));
            if(cmd != null)
                wd = cmd.execute(wd, cmdargs);
            else {
                System.err.print("Command \"");
                System.err.print(cmdargs.get(0));
                System.err.println("\" not found!");
            }
            cmdargs.clear();
        }
    }
}
