
public class Producer implements Runnable {
    private Fifo fifo;
    private String msg;
    private int sleep;
    
    public Producer(Fifo fifo, String msg, int sleep) {
        this.fifo = fifo;
        this.msg = msg;
        this.sleep = sleep;
    }
    
    @Override
    public void run() {
        int i = 0;
//        while(true) {
            fifo.put(msg+' '+i);
            
            synchronized(System.out) {
                System.out.print("produced ");
                System.out.print(msg);
                System.out.print(' ');
                System.out.print(i++);
                System.out.print(' ');
                System.out.println(System.currentTimeMillis()%10000);
            }
            
            try {
                Thread.sleep(sleep);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
//        }
    }
}
