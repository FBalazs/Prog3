import java.util.ArrayList;

public class Fifo {
    private ArrayList<String> data = new ArrayList<>(10);
    
    public synchronized void put(String str) {
        System.out.println("put "+Thread.currentThread().getName());
        while(data.size() >= 10)
            try {
                wait();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        data.add(str);
        notifyAll();
    }
    
    public synchronized String get() {
        System.out.println("get "+Thread.currentThread().getName());
        while(data.isEmpty())
            try {
                wait();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        notifyAll();
        return data.remove(0);
    }
}
