import java.util.concurrent.ScheduledThreadPoolExecutor;

public class Application {
    public static void main(String[] args) {
        Fifo fifo = new Fifo();
        
        ScheduledThreadPoolExecutor stpeCons = new ScheduledThreadPoolExecutor(10),
                                    stpeProd = new ScheduledThreadPoolExecutor(10);
        
        for(int p = 0; p < 15; ++p)
            stpeCons.execute(new Producer(fifo, "p"+p, 1000));
        for(int c = 0; c < 15; ++c)
            stpeProd.execute(new Consumer(fifo, "c"+c, 1000));
        stpeCons.shutdown();
        stpeProd.shutdown();
        
//        try {
//            Thread.sleep(10000);
//        } catch(InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("slept enough");
        
//      for(int p = 0; p < 3; ++p)
//          new Thread(new Producer(fifo, "p"+p, 1000)).start();
//      for(int c = 0; c < 4; ++c)
//          new Consumer(fifo, "c"+c, 100).start();
        
//        new Thread(new Producer(fifo, "vmi", 500)).start();
//        new Consumer(fifo, "", 1000).start();
        
//        new Producer("elso   ", fifo).start();
//        
//        try {
//            Thread.sleep(500);
//        } catch(InterruptedException e) {
//            e.printStackTrace();
//        }
//        
//        new Producer("masodik", fifo).start();
    }
}
