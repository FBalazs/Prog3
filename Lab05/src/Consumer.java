
public class Consumer extends Thread {
    private Fifo fifo;
    private String str;
    private int sleep;
    
    public Consumer(Fifo fifo, String str, int sleep) {
        this.fifo = fifo;
        this.str = str;
        this.sleep = sleep;
    }
    
    @Override
    public void run() {
        String read;
//        while(true) {
            read = fifo.get();
            
            synchronized(System.out) {
                System.out.print("consumed ");
                System.out.print(str);
                System.out.print(" \"");
                System.out.print(read);
                System.out.print("\" ");
                System.out.println(System.currentTimeMillis()%10000);
            }
            
            try {
                Thread.sleep(sleep);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
//        }
    }
}
