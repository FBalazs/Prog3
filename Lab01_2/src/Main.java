import sample.calc.Calculator;

public class Main {
	public static void main(String[] args) {
		Calculator calc = new Calculator();
		int sum = 0;
		for(String arg : args)
			sum = calc.add(sum, Integer.parseInt(arg));
		System.out.println(sum);
	}
}
