package swingmvclab;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/*
 * A hallgat�k adatait t�rol� oszt�ly.
 */
public class StudentData extends AbstractTableModel {

    /*
     * Ez a tagv�ltoz� t�rolja a hallgat�i adatokat.
     * NE M�DOS�TSD!
     */
    List<Student> students = new ArrayList<Student>();

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        return students.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex) {
            case 0:
                return students.get(rowIndex).getName();
            case 1:
                return students.get(rowIndex).getNeptun();
            case 2:
                return students.get(rowIndex).hasSignature();
            case 3:
                return students.get(rowIndex).getGrade();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0:
                return "N�v";
            case 1:
                return "Neptun";
            case 2:
                return "Al��r�s";
            case 3:
                return "Jegy";
            default:
                return super.getColumnName(column);
        }
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return Boolean.class;
            case 3:
                return Integer.class;
            default:
                return super.getColumnClass(columnIndex);
        }
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 2 || columnIndex == 3;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch(columnIndex) {
            case 2:
                students.get(rowIndex).setSignature((Boolean)aValue);
                break;
            case 3:
                students.get(rowIndex).setGrade((Integer)aValue);
                break;
        }
        super.setValueAt(aValue, rowIndex, columnIndex);
    }
    
    public void addStudent(String name, String neptun) {
        students.add(new Student(name, neptun, false, 0));
        fireTableRowsInserted(students.size()-1, students.size()-1);
    }
}
