import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class FileManager {
    static interface Command {
        public File execute(File wd, List<String> args) throws IOException;
        public String help();
        public String name();
    }
    
    static HashMap<String, Command> commands = new HashMap<>();
    
    static void addCommand(Command cmd) {
        commands.put(cmd.name(), cmd);
    }
    
    static {
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                if(1 < args.size()) {
                    Command cmd = commands.get(args.get(1));
                    if(cmd == null) {
                        System.err.print("Command \"");
                        System.err.print(args.get(1));
                        System.err.println("\" not found!");
                    } else {
                        System.out.println(cmd.help());
                    }
                } else {
                    for(String str : commands.keySet())
                        System.out.println(str);
                }
                return wd;
            }

            @Override
            public String help() {
                return "help [<cmd>]\nIf <cmd> is present prints the usage of <cmd> otherwise lists all the available commands.";
            }

            @Override
            public String name() {
                return "help";
            }
        });
        
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                System.exit(0);
                return wd;
            }

            @Override
            public String help() {
                return "exit\nCloses the application.";
            }

            @Override
            public String name() {
                return "exit";
            }
        });
        
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                System.out.println(wd.getCanonicalPath());
                return wd;
            }
            
            @Override
            public String help() {
                return "pwd\nPrints the current working directory.";
            }

            @Override
            public String name() {
                return "pwd";
            }
        });
        
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                if(args.size() < 2) {
                    System.err.println("cd needs at least 1 parameter!");
                    return wd;
                }
                
                if(args.get(1).equals(".."))
                    return wd.getParentFile();
                
                File nwd = new File(wd, args.get(1));
                if(nwd.exists() && nwd.isDirectory())
                    return nwd;
                System.err.println("Invalid directory!");
                return wd;
            }
            
            @Override
            public String help() {
                return "cd <dir>\nChanges the working directory. If <dir> is \"..\" the working directory will change to it's parent directory.";
            }

            @Override
            public String name() {
                return "cd";
            }
        });
        
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                if(args.contains("-l")) {
                    File[] files = wd.listFiles();
                    int maxlength = 0;
                    for(File file : files)
                        if(maxlength < file.getName().length())
                            maxlength = file.getName().length();
                    for(File file : files) {
                        System.out.print(file.getName());
                        for(int i = file.getName().length(); i < maxlength; ++i)
                            System.out.print(" ");
                        System.out.print(file.isDirectory() ? "\td\t" : "\tf\t");
                        System.out.print(file.length());
                        System.out.println(" bytes");
                    }
                } else {
                    String[] files = wd.list();
                    for(String str : files)
                        System.out.println(str);
                }
                
                return wd;
            }
            
            @Override
            public String help() {
                return "ls [-l]\nLists the files and directories in the working directory. If \"-l\" is present it will also print the type of the files and their size.";
            }

            @Override
            public String name() {
                return "ls";
            }
        });
        
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                if(args.size() < 3) {
                    System.err.println("mv needs at least 2 parameters!");
                    return wd;
                }
                
                File f = new File(wd, args.get(1));
                if(!f.exists()) {
                    System.err.print(args.get(1));
                    System.err.println(" does not exist!");
                } else if(!f.renameTo(new File(wd, args.get(2)))) {
                    System.err.print("Could not rename ");
                    System.err.print(args.get(1));
                    System.err.print(" to ");
                    System.err.print(args.get(2));
                    System.err.println("!");
                }
                return wd;
            }
            
            @Override
            public String help() {
                return "mv <file1> <file2>\nRenames <file1> to <file2>.";
            }

            @Override
            public String name() {
                return "mv";
            }
        });
        
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                if(args.size() < 2) {
                    System.err.println("cat needs at least 1 parameter!");
                    return wd;
                }
                
                File f = new File(wd, args.get(1));
                if(!f.exists()) {
                    System.err.print(args.get(1));
                    System.err.println(" does not exist!");
                } else {
                    FileReader fr = new FileReader(f);
                    int i = fr.read();
                    while(i != -1) {
                        System.out.print((char)i);
                        i = fr.read();
                    }
                    fr.close();
                }
                
                return wd;
            }
            
            @Override
            public String help() {
                return "cat <file>\nPrints the <file>'s contents.";
            }

            @Override
            public String name() {
                return "cat";
            }
        });
        
        addCommand(new Command() {
            @Override
            public File execute(File wd, List<String> args) throws IOException {
                if(args.size() < 2) {
                    System.err.println("wc needs at least 1 parameter!");
                    return wd;
                }
                
                File f = new File(wd, args.get(1));
                if(!f.exists()) {
                    System.err.print(args.get(1));
                    System.err.println(" does not exist!");
                } else {
                    int lines = 0, words = 0, letters = 0;
                    BufferedReader br = new BufferedReader(new FileReader(f));
                    String line = br.readLine();
                    while(line != null) {
                        ++lines;
                        StringTokenizer st = new StringTokenizer(line);
                        while(st.hasMoreTokens()) {
                            ++words;
                            letters += st.nextToken().length();
                        }
                        
                        line = br.readLine();
                    }
                    br.close();
                    System.out.print(lines);
                    System.out.print(" lines, ");
                    System.out.print(words);
                    System.out.print(" words, ");
                    System.out.print(letters);
                    System.out.println(" letters");
                }
                
                return wd;
            }
            
            @Override
            public String help() {
                return "wc <file>\nPrints the number of lines, words and letters in <file>.";
            }

            @Override
            public String name() {
                return "wc";
            }
        });
    }
    
    static Reader reader;
    static Writer writer;
    static File wd;
    
    public static void main(String[] args) throws IOException {
        reader = new InputStreamReader(System.in);
        writer = new OutputStreamWriter(System.out);
        
        wd = new File(System.getProperty("user.dir"));
        //wd = new File("");
        
        StringBuilder sb = new StringBuilder();
        ArrayList<String> cmdargs = new ArrayList<>();
        char c;
        
        boolean flag;
        boolean inquotes;
        
        while(true) {
            flag = true;
            inquotes = false;
            
            while(flag)  {
                c = (char)reader.read();
                
                if(c == '\n' || c == '\r') {
                    if(sb.length() != 0) {
                        cmdargs.add(sb.toString());
                        sb.setLength(0);
                        flag = false;
                    }
                } else if(!inquotes && c == ' ') {
                    cmdargs.add(sb.toString());
                    sb.setLength(0);
                } else if(c == '\"') {
                    inquotes = !inquotes;
                } else {
                    sb.append(c);
                }
            }
            Command cmd = commands.get(cmdargs.get(0));
            if(cmd != null)
                wd = cmd.execute(wd, cmdargs);
            else {
                System.err.print("Command \"");
                System.err.print(cmdargs.get(0));
                System.err.println("\" not found!");
            }
            cmdargs.clear();
        }
    }
}
