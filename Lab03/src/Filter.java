import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Filter {
    static InputStream ins;
    static OutputStream outs;
    static BufferedReader br;
    static PrintWriter pw;
    
    public static void main(String[] args) throws Exception {
        ins = null;
        outs = null;

        String pattern = "(asdf)*";
        boolean zipin = false, zipout = false;
        for(int i = 0; i < args.length; ++i) {
            if((i + 1 < args.length) && args[i].equals("-p"))
                pattern = args[++i];
            else if((i + 1 < args.length) && args[i].equals("-i"))
                ins = new FileInputStream(args[++i]);
            else if((i + 1 < args.length) && args[i].equals("-o"))
                outs = new FileOutputStream(args[++i]);
            else if(args[i].equals("-gi"))
                zipin = true;
            else if(args[i].equals("-go"))
                zipout = true;
        }
        
        if(ins == null)
            ins = System.in;
        if(outs == null)
            outs = System.out;
        
        if(zipin)
            ins = new GZIPInputStream(ins);
        if(zipout)
            outs = new GZIPOutputStream(outs);
        
        br = new BufferedReader(new InputStreamReader(ins));
        pw = new PrintWriter(outs);
        
        String line;
        while((line = br.readLine()) != null) {
            if(line.matches(pattern)) {
                pw.println(line);
                pw.flush();
            }
        }
        
        br.close();
        pw.close();
    }
}
