public class Calculator {
    static long _d = 1L;
    int n;

    public Calculator() {
        this.n = 0;
    }

    public int work() throws InterruptedException {
        _d += _d / 2L + _d % 7L;
        _d %= 1000L;
        Thread.sleep(10L);
        return this.n++;
    }

    public double get() {
        return _d;
    }
}