import javax.swing.SwingWorker;

public class Updater extends SwingWorker<Double, Object> {

    @Override
    public Double doInBackground() {
        Calculator calc = new Calculator();
        while(getProgress() < 100 && !isCancelled()) {
            try {
                setProgress(calc.work());
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        return calc.get();
    }
}
