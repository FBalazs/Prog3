import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class Labor implements Runnable, ActionListener, PropertyChangeListener {

    /* a k�t updater, ha �ppen futnak */
    Updater u1;
    Updater u2;

    public Labor() {
        /* kezdetben nincs munka */
        u1 = null;
        u2 = null;
    }

    /** a gombokt�l j�v� esem�nyek kezel�se */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()) {
            case "u1":
                if(u1 != null)
                    u1.cancel(true);
                u1 = new Updater();
                u1.addPropertyChangeListener(this);
                u1.execute();
                break;
            case "u2":
                if(u2 != null)
                    u2.cancel(true);
                u2 = new Updater();
                u2.addPropertyChangeListener(this);
                u2.execute();
                break;
        }
    }

    /** az updater-ekt�l j�v� esem�nyek kezel�se */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        try {
            if(evt.getSource() == u1) {
                progress1.setValue(u1.getProgress());
                if(u1.isDone())
                    field1.setText(u1.get().toString());
            } else if(evt.getSource() == u2) {
                progress2.setValue(u2.getProgress());
                if(u2.isDone())
                    field2.setText(u2.get().toString());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /* az esem�nykezel�s sor�n el�rend� elemek */
    JTextField field1;
    JProgressBar progress1;
    JTextField field2;
    JProgressBar progress2;

    JButton btn1, btn2;

    /**
     * a frame-et fel�p�t� k�d
     * 
     * @wbp.parser.entryPoint
     */
    @Override
    public void run() {
        /* frame l�trehoz�sa */
        JFrame f = new JFrame();
        /* layout hozz�rendel�se. M�g nincs meg a kioszt�s!!! */
        GroupLayout layout = new GroupLayout(f.getContentPane());
        f.getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        /* a fels� sor elemei */
        JLabel label1 = new JLabel("First row");
        field1 = new JTextField(10);
        progress1 = new JProgressBar(0, 99);
        btn1 = new JButton("Update");
        btn1.setActionCommand("u1");
        btn1.addActionListener(this);

        /* az als� sor elemei */
        JLabel label2 = new JLabel("Second row");
        field2 = new JTextField(10);
        progress2 = new JProgressBar(0, 99);
        btn2 = new JButton("Update");
        btn2.setActionCommand("u2");
        btn2.addActionListener(this);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup().addComponent(label1).addComponent(label2))
                .addGroup(layout.createParallelGroup().addComponent(field1).addComponent(field2))
                .addGroup(layout.createParallelGroup().addComponent(progress1).addComponent(progress2))
                .addGroup(layout.createParallelGroup().addComponent(btn1).addComponent(btn2)));

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup().addComponent(label1, Alignment.CENTER)
                        .addComponent(field1, Alignment.CENTER).addComponent(progress1, Alignment.CENTER)
                        .addComponent(btn1, Alignment.CENTER))
                .addGroup(layout.createParallelGroup().addComponent(label2, Alignment.CENTER)
                        .addComponent(field2, Alignment.CENTER).addComponent(progress2, Alignment.CENTER)
                        .addComponent(btn2, Alignment.CENTER)));

        /* a textfield-ek nem ny�lnak f�gg�legesen */
        layout.linkSize(SwingConstants.VERTICAL, field1, field2);

        /* a frame be�ll�t�sa */
        f.pack();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setTitle("Swing Labor");
        f.setVisible(true);
    }

    static public void main(String[] args) {
        SwingUtilities.invokeLater(new Labor());
    }
}
