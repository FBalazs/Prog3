
public abstract class Asztal {
    public static class NincsJatekos extends Exception {
        private static final long serialVersionUID = -5396852503655817093L;
        
    }
    
    private double tet = 0;
    private int korok = 0;
    
    public void ujJatek() {
        tet = 0;
        korok = 0;
    }
    
    public abstract void addJatekos(Jatekos j);
    
    public int getKor() {
        return korok;
    }
    
    public void emel(double d) {
        tet += d;
    }
    
    public void kor() throws NincsJatekos {
        ++korok;
        System.out.println("A t�t aktu�lis �rt�ke "+tet+" pet�k.");
    }
}
