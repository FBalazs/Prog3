import java.lang.ref.WeakReference;
import java.util.LinkedList;

public class WeakAsztal extends Asztal {
private LinkedList<WeakReference<Jatekos>> jatekosok = new LinkedList<>();
    
    @Override
    public void addJatekos(Jatekos j) {
        j.setAsztal(this);
        jatekosok.add(new WeakReference<Jatekos>(j));
    }
    
    @Override
    public void kor() throws NincsJatekos {
        if(jatekosok.size() == 0)
            throw new NincsJatekos();
        
        for(WeakReference<Jatekos> ref : jatekosok) {
            Jatekos j = ref.get();
            if(j != null)
                j.lep();
            else
                System.err.println("NULL :)");
        }
        super.kor();
    }
}
