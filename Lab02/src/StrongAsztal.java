import java.util.LinkedList;

public class StrongAsztal extends Asztal {
    private LinkedList<Jatekos> jatekosok = new LinkedList<>();
    
    @Override
    public void addJatekos(Jatekos j) {
        j.setAsztal(this);
        jatekosok.add(j);
    }
    
    @Override
    public void kor() throws NincsJatekos {
        if(jatekosok.size() == 0)
            throw new NincsJatekos();
        
        for(Jatekos j : jatekosok)
            j.lep();
        super.kor();
    }
}
