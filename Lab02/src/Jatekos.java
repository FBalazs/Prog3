
public abstract class Jatekos {
    /**
     * utols� azonos�t�
     */
    private static int lastid = 0;
    
    /**
     * az asztal :)
     */
    protected Asztal asztal = null;
    /**
     * azonos�t�
     */
    private final int id;
    
    /**
     * Konstruktor j������j.
     */
    public Jatekos() {
        id = ++lastid;
    }
    
    /**
     * lek�rAzonos�t�
     * @return az azonos�t�
     */
    public int getId() {
        return id;
    }
    
    public void lep() {
        System.out.println(asztal.getKor()+". k�r: "+this.toString());
    }
    
    public void setAsztal(Asztal a) {
        asztal = a;
    }
    
    @Override
    protected void finalize() {
        System.out.println("finalize(): "+id+" "+toString());
    }
}
