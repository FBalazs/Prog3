
public class Main {
    /**
     * Main method.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        Asztal a = new StrongAsztal();
        a.addJatekos(new Kezdo());
        a.addJatekos(new Kezdo());
        a.addJatekos(new Robot());
        
        a.ujJatek();
        for(int i = 0; i < 3; ++i)
            try {
                a.kor();
            } catch(Asztal.NincsJatekos e) {
                e.printStackTrace();
            }
        
        Asztal a2 = new StrongAsztal();
        try {
            a2.kor();
        } catch(Asztal.NincsJatekos e) {
            e.printStackTrace();
        }
        
        a = null;
        a2 = null;
        System.gc();
        try {
            Thread.sleep(1000);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("after gc");
//        Jatekos j;
//        j.
        
        //StrongAsztal asztal = new StrongAsztal();
        WeakAsztal asztal = new WeakAsztal();
        for(long i = Long.MIN_VALUE; i < Long.MAX_VALUE; ++i)
            asztal.addJatekos(new Kezdo());
    }
}
