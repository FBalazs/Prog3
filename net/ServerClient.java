package fb.net;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.LinkedList;

public class ServerClient {
    private Server server = null;
    private Socket tcpSocket = null;
    
    private ObjectInputStream tcpIn = null;
    private ObjectOutputStream tcpOut = null;
    
    private Object tcpSendLock = new Object();
    private LinkedList<Object> tcpSendQueue = null;
    
    private boolean open = false;
    
    public synchronized void setServer(Server server) {
        this.server = server;
    }
    
    public synchronized void setTcpSocket(Socket tcpSocket) {
        this.tcpSocket = tcpSocket;
    }
    
    public synchronized SocketAddress getRemoteSocketAddress() {
        return tcpSocket == null ? null : tcpSocket.getRemoteSocketAddress();
    }
    
    public synchronized InetAddress getRemoteInetAddress() {
        return tcpSocket == null ? null : tcpSocket.getInetAddress();
    }
    
    public synchronized int getRemotePort() {
        return tcpSocket == null ? -1 : tcpSocket.getPort();
    }
    
    public synchronized int getLocalPort() {
        return tcpSocket == null ? -1 : tcpSocket.getLocalPort();
    }
    
    public synchronized void open() {
        if(open)
            close();
        try {
            tcpIn = new ObjectInputStream(tcpSocket.getInputStream());
            tcpOut = new ObjectOutputStream(tcpSocket.getOutputStream());
            
            tcpSendQueue = new LinkedList<>();
        } catch (Exception e) {
            close(e);
        } finally {
            open = true;
            
            new Thread(tcpSendThread).start();
            new Thread(tcpReceiveThread).start();
            
            if(server.getListener() != null)
                server.getListener().onClientConnected(server, this);
        }
    }
    
    private Runnable tcpSendThread = new Runnable() {
        @Override
        public void run() {
            while(open) {
                try {
                    synchronized(tcpSendLock) {
                        while(!tcpSendQueue.isEmpty())
                            tcpOut.writeObject(tcpSendQueue.removeFirst());
                        tcpOut.flush();
                        tcpSendLock.wait();
                    }
                } catch(Exception e) {
                    close(e);
                }
            }
        }
    };
    
    private Runnable tcpReceiveThread = new Runnable() {
        @Override
        public void run() {
            Object o;
            while(open) {
                try {
                    o = tcpIn.readObject();
                    if(o != null && server.getListener() != null)
                        server.getListener().onTcpReceived(server, ServerClient.this, o);
                } catch (Exception e) {
                    close(e);
                }
            }
        }
    };
    
    public synchronized void close(Exception e) {
        if(!open)
            return;
        
        if(tcpIn != null) {
            try {
                tcpIn.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            tcpIn = null;
        }
        
        if(tcpOut != null) {
            try {
                tcpOut.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            tcpOut = null;
        }
        
        if(tcpSocket != null) {
            try {
                tcpSocket.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            tcpSocket = null;
        }
        
        tcpSendQueue = null;
        
        open = false;
        
        server.removeClient(this);
        
        if(server.getListener() != null)
            if(e == null)
                server.getListener().onClientClosed(server, this);
            else
                server.getListener().onClientConnectionLost(server, this, e);
    }
    
    public synchronized void close() {
        close(null);
    }
}
