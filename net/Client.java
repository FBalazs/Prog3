package fb.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.LinkedList;

public class Client {private InetSocketAddress address   = null;
    private boolean           connected = false;

    private Socket         tcpSocket = null;
    private DatagramSocket udpSocket = null;

    private ObjectInputStream  tcpIn  = null;
    private ObjectOutputStream tcpOut = null;

    private Object             tcpSendLock  = new Object();
    private LinkedList<Object> tcpSendQueue = null;

    private Object         udpSendLock   = new Object();
    private DatagramPacket udpSendPacket = null;

    private ClientListener listener = null;

    public synchronized void setListener(ClientListener listener) {
        this.listener = listener;
    }

    public synchronized ClientListener getListener() {
        return listener;
    }

    public synchronized boolean isConnected() {
        return connected;
    }

    public synchronized InetSocketAddress getServerAddress() {
        return address;
    }

    public synchronized int getLocalPort() {
        return tcpSocket == null ? -1 : tcpSocket.getLocalPort();
    }

    public synchronized void connect(String host, int port, int timeout) {
        if(connected)
            close();

        try {
            address = new InetSocketAddress(host, port);
            tcpSocket = new Socket();
            tcpSocket.setKeepAlive(true);
            tcpSocket.connect(address, timeout);

            udpSocket = new DatagramSocket(tcpSocket.getLocalPort());
            udpSocket.connect(address);

            udpSendPacket = new DatagramPacket(new byte[1], 1, address);

            tcpOut = new ObjectOutputStream(tcpSocket.getOutputStream());
            tcpIn = new ObjectInputStream(tcpSocket.getInputStream());
            
            tcpSendQueue = new LinkedList<>();
            
            connected = true;

            new Thread(tcpSendThread).start();
            new Thread(tcpReceiveThread).start();
            new Thread(udpReceiveThread).start();

            if(listener != null)
                listener.onConnected(Client.this);
        } catch(Exception e) {
            close(e);
        }
    }

    private Runnable tcpSendThread = new Runnable() {
        @Override
        public void run() {
            while(connected) {
                try {
                    synchronized(tcpSendLock) {
                        while(!tcpSendQueue.isEmpty())
                            tcpOut.writeObject(tcpSendQueue.removeFirst());
                        tcpOut.flush();
                        tcpSendLock.wait();
                    }
                } catch(Exception e) {
                    close(e);
                }
            }
        }
    };

    private Runnable tcpReceiveThread = new Runnable() {
        @Override
        public void run() {
            Object o;
            while(connected) {
                try {
                    o = tcpIn.readObject();
                    if(o != null && listener != null)
                        listener.onTcpReceived(Client.this, o);
                } catch(Exception e) {
                    close(e);
                }
            }
        }
    };

    private Runnable udpReceiveThread = new Runnable() {
        @Override
        public void run() {
            byte[] buffer = new byte[1024];
            DatagramPacket dp = new DatagramPacket(buffer, buffer.length);

            ObjectInputStream ois;

            Object o;

            while(connected) {
                try {
                    udpSocket.receive(dp);

                    ois = new ObjectInputStream(new ByteArrayInputStream(buffer));
                    o = ois.readObject();
                    ois.close();

                    if(o != null && dp.getSocketAddress().equals(address) && listener != null)
                        listener.onUdpReceived(Client.this, o);
                } catch(Exception e) {
                    close(e);
                }
            }
        }
    };

    public void sendTcp(Object o) {
        try {
            synchronized(tcpSendLock) {
                tcpSendQueue.add(o);
                tcpSendLock.notifyAll();
            }
        } catch(Exception e) {
            close(e);
        }
    }

    public void sendUdp(Object o) {
        try {
            synchronized(udpSendLock) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(o);
                udpSendPacket.setData(baos.toByteArray());
                udpSocket.send(udpSendPacket);
                oos.close();
            }
        } catch(Exception e) {
            close(e);
        }
    }

    public synchronized void close(Exception e) {
        if(!connected)
            return;

        address = null;

        if(tcpIn != null) {
            try {
                tcpIn.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            tcpIn = null;
        }

        if(tcpOut != null) {
            try {
                tcpOut.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            tcpOut = null;
        }

        if(tcpSocket != null) {
            try {
                tcpSocket.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            tcpSocket = null;
        }

        if(udpSocket != null) {
            try {
                udpSocket.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            udpSocket = null;
        }

        tcpSendQueue = null;

        connected = false;

        if(listener != null)
            if(e == null)
                listener.onClosed(this);
            else
                listener.onConnectionLost(this, e);
    }

    public synchronized void close() {
        close(null);
    }
}
