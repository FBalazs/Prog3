package fb.net;

public interface ServerListener {
    public void onOpened(Server server);

    public void onFailed(Server server, Exception e);

    public void onClosed(Server server);

    public void onClientConnected(Server server, ServerClient client);

    public void onClientConnectionLost(Server server, ServerClient client, Exception e);

    public void onClientClosed(Server server, ServerClient client);

    public void onTcpReceived(Server server, ServerClient client, Object o);

    public void onUdpReceived(Server server, ServerClient client, Object o);
}
