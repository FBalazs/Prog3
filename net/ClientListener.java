package fb.net;

public interface ClientListener {
    public void onConnected(Client client);

    public void onConnectionLost(Client client, Exception e);

    public void onClosed(Client client);

    public void onTcpReceived(Client client, Object o);

    public void onUdpReceived(Client client, Object o);
}
