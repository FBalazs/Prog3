package fb.net;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

import fb.util.WeakPool;

public class Server {
    private boolean open = false;

    private ServerSocket   tcpSocket = null;
    private DatagramSocket udpSocket = null;

    private LinkedList<ServerClient> clients = new LinkedList<>();
    private WeakPool<ServerClient>   clientPool;

    private ServerListener listener = null;

    public Server() {
        try {
            clientPool = new WeakPool<>(ServerClient.class.getConstructor());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void setListener(ServerListener listener) {
        this.listener = listener;
    }

    public synchronized ServerListener getListener() {
        return listener;
    }

    public synchronized boolean isOpen() {
        return open;
    }

    public synchronized int getPort() {
        return tcpSocket == null ? -1 : tcpSocket.getLocalPort();
    }

    public synchronized void addClient(ServerClient client) {
        synchronized(clients) {
            clients.add(client);
        }
    }

    public synchronized void removeClient(ServerClient client) {
        synchronized(clients) {
            clients.remove(client);   
        }
    }

    public synchronized void open(int port) {
        if(open)
            close();

        try {
            tcpSocket = new ServerSocket(port);
            udpSocket = new DatagramSocket(port);
            
            open = true;

            new Thread(udpReceiveThread).start();
            new Thread(listenThread).start();

            if(listener != null)
                listener.onOpened(this);
        } catch(Exception e) {
            close(e);
        }
    }

    private Runnable udpReceiveThread = new Runnable() {
        @Override
        public void run() {
            byte[] buffer = new byte[1024];
            DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
            
            ServerClient client;
            
            ObjectInputStream ois;
            Object o;

            while(open) {
                try {
                    udpSocket.receive(dp);

                    ois = new ObjectInputStream(new ByteArrayInputStream(buffer));
                    o = ois.readObject();
                    ois.close();

                    if(o != null && listener != null) {
                        client = null;
                        synchronized(clients) {
                            for(ServerClient cc : clients)
                                if(dp.getSocketAddress().equals(cc.getRemoteSocketAddress()))
                                    client = cc;
                        }
                        listener.onUdpReceived(Server.this, client, o);
                    }
                } catch(Exception e) {
                    close(e);
                }
            }
        }
    };

    private Runnable listenThread=new Runnable(){
        @Override
        public void run(){
            Socket socket;
            ServerClient client;
            while(open) {
                try {
                    socket = tcpSocket.accept();
                    if(socket != null) {
                        client = clientPool.get();
                        if(client == null)
                            client = new ServerClient();
                        client.setServer(Server.this);
                        client.setTcpSocket(socket);
                        addClient(client);
                        client.open();
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public synchronized void close(Exception e) {
        if(!open)
            return;

        if(tcpSocket != null) {
            try {
                tcpSocket.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            tcpSocket = null;
        }

        if(udpSocket != null) {
            try {
                udpSocket.close();
            } catch(Exception e1) {
                e1.printStackTrace();
            }
            udpSocket = null;
        }

        for(ServerClient client : clients)
            client.close();
        clients.clear();

        open = false;
        if(listener != null)
            if(e == null)
                listener.onClosed(this);
            else
                listener.onFailed(this, e);
    }

    public synchronized void close() {
        close(null);
    }
}
