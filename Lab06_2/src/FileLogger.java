import java.io.FileWriter;
import java.io.IOException;

public class FileLogger implements Observer {
    private FileWriter fw;
    
	public FileLogger(String fileName) {
	    try {
            fw = new FileWriter(fileName);
        } catch(IOException e) {
            e.printStackTrace();
        }
	}

    @Override
    public void report(Observable observable) {
        try {
            fw.write(observable.toString());
            fw.write('\n');
            fw.flush();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
