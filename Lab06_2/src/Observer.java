public interface Observer {
	public void report(Observable observable);
}
