public class PercentCounter extends Observable {
	private int percent;
	
	public void run() {
	    for(percent = 0; percent < 100; ++percent)
	        if(percent%10 == 0)
	            reportToObservers();
	    reportToObservers();
	}
	
	@Override
	public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append(percent).append('%');
	    return sb.toString();
	}
}
