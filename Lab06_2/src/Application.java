public class Application {
	public static void main(String[] args) {
	    PercentCounter pc = new PercentCounter();
	    StdOutLogger sol = new StdOutLogger();
	    FileLogger fl = new FileLogger("observer.txt");
	    pc.register(sol);
	    pc.register(fl);
	    pc.run();
	}
}
