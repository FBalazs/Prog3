import java.util.LinkedList;

public class Observable {
	private LinkedList<Observer> observers = new LinkedList<>();
	
	public void register(Observer observer) {
	    observers.add(observer);
	}
	
	public void unregister(Observer observer) {
	    observers.remove(observer);
	}
	
	public void reportToObservers() {
	    for(Observer o : observers)
	        o.report(this);
	}
}
