package fb.util;

public class Time {
    public static final int ONE_SECOND = 1000000000;
    
    public static long getTime() {
        return System.nanoTime();
    }
}
