package fb.util;

import java.lang.reflect.Constructor;
import java.util.LinkedList;

public class Pool<T> implements IPool<T> {
    private final Constructor<T> constructor;
    private final Object[] args;
    private final LinkedList<T> objects = new LinkedList<>();
    
    public Pool(Constructor<T> constructor, Object... args) {
        this.constructor = constructor;
        this.args = args;
    }
    
    @Override
    public synchronized void add(T object) {
        objects.add(object);
    }
    
    @Override
    public synchronized T get() {
        if(objects.isEmpty())
            try {
                return constructor.newInstance(args);
            } catch(Exception e) {
                e.printStackTrace();
                return null;
            }
        else
            return objects.removeFirst();
    }
    
    @Override
    public synchronized void clear() {
        objects.clear();
    }
}
