package fb.util;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.util.LinkedList;

public class WeakPool<T> implements IPool<T> {
    private final Constructor<T> constructor;
    private final Object[] args;
    private final LinkedList<WeakReference<T>> objects = new LinkedList<>();
    
    public WeakPool(Constructor<T> constructor, Object... args) {
        this.constructor = constructor;
        this.args = args;
    }
    
    @Override
    public synchronized void add(T object) {
        objects.add(new WeakReference<T>(object));
    }
    
    @Override
    public synchronized T get() {
        T ret = null;
        while(!objects.isEmpty()) {
            ret = objects.removeFirst().get();
            if(ret != null)
                return ret;
        }
        
        try {
            return constructor.newInstance(args);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public synchronized void clear() {
        objects.clear();
    }
}
