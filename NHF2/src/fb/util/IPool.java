package fb.util;

public interface IPool<T> {
    public void add(T object);
    
    public T get();
    
    public void clear();
}
