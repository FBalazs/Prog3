package fb.util;

public class GameLoopThread implements Runnable {
    public static interface IGameLoopCallback {
        public void init();
        
        public void update(float delta);
        
        public void render(float delta);

        public void dispose();
    }

    public static class GameLoopCallback implements IGameLoopCallback {
        @Override
        public void init() {
        }

        @Override
        public void update(float delta) {
        }

        @Override
        public void render(float delta) {
        }

        @Override
        public void dispose() {
        }
    }

    private IGameLoopCallback callback;
    
    private boolean running = false;
    private Thread thread = null;

    private long tickStartTime, lastClockTime, lastUpdateTime, lastRenderTime;
    private int updateTime = Time.ONE_SECOND / 50, renderTime = Time.ONE_SECOND / 120;
    private int cups, ups, crps, rps;
    private float updateDelta, renderDelta;

    public GameLoopThread(IGameLoopCallback callback) {
        this.callback = callback;
    }

    public long getTickStartTime() {
        return tickStartTime;
    }

    public int getUPS() {
        return ups;
    }

    public void setRUPS(int rups) {
        updateTime = Time.ONE_SECOND / rups;
    }

    public int getRPS() {
        return rps;
    }

    public void setRRPS(int rrps) {
        renderTime = Time.ONE_SECOND / rrps;
    }

    public void start() {
        if(running)
            stop();

        running = true;

        thread = new Thread(this);
        thread.start();
    }

    public void stop() {
        if(!running)
            return;

        running = false;

        thread.interrupt();
        try {
            thread.join();
        } catch(InterruptedException e) {
//            e.printStackTrace(System.out);
        }
    }

    @Override
    public void run() {
        try {
            lastClockTime = Time.getTime() - Time.ONE_SECOND;
            lastUpdateTime = lastRenderTime = Time.getTime();
            cups = ups = crps = rps = 0;
            callback.init();
            long sleepTime;
            running = true;
            while(running) {
                tickStartTime = Time.getTime();
                if(lastClockTime + Time.ONE_SECOND <= tickStartTime) {
                    ups = cups;
                    rps = crps;
                    cups = crps = 0;
                    lastClockTime = tickStartTime;
                }

                if(lastUpdateTime + updateTime <= tickStartTime) {
                    updateDelta = (tickStartTime - lastUpdateTime) / (float)Time.ONE_SECOND;
                    callback.update(updateDelta);
                    ++cups;
                    lastUpdateTime = tickStartTime;
                }

                if(lastRenderTime + renderTime <= tickStartTime) {
                    renderDelta = (tickStartTime - lastUpdateTime) / (float)Time.ONE_SECOND;
                    callback.render(renderDelta);
                    ++crps;
                    lastRenderTime = tickStartTime;
                }

                sleepTime = Math.min(lastUpdateTime + updateTime, lastRenderTime + renderTime) - Time.getTime();
                if(0 < sleepTime)
                    try {
                        Thread.sleep(sleepTime * 1000 / Time.ONE_SECOND);
                    } catch(InterruptedException e) {
//                        e.printStackTrace(System.out);
                    }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        callback.dispose();
    }
}
