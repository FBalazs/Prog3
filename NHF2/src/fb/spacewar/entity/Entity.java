package fb.spacewar.entity;

import java.awt.Color;

import fb.spacewar.SpaceWarGame;
import fb.spacewar.display.Renderer;

/**
 * Represents a moving object in the world.
 */
public class Entity implements Cloneable {
    private static final boolean RENDERBOUNDING = false;
    
    /**
     * Game reference.
     */
    private SpaceWarGame game;
    
    /**
     * X coordinate of the object.
     */
    protected float x = 0;
    /**
     * Y coordinate of the object.
     */
    protected float y = 0;
    /**
     * Next X coordinate of the object.
     */
    protected float nx = 0;
    /**
     * Next Y coordinate of the object.
     */
    protected float ny = 0;
    /**
     * Velocity of the object in the direction of the X axis.
     */
    protected float vx = 0;
    /**
     * Velocity of the object in the direction of the Y axis.
     */
    protected float vy = 0;
    /**
     * Next value of vx.
     */
    protected float nvx = 0;
    /**
     * Next value of vy.
     */
    protected float nvy = 0;
    /**
     * Acceleration of the object in the direction of the X axis.
     */
    protected float ax = 0;
    /**
     * Acceleration of the object in the direction of the Y axis.
     */
    protected float ay = 0;
    /**
     * Rotation of the object in radians.
     */
    protected float rot = 0;
    /**
     * Radius of the object's bounding circle.
     */
    protected float radius;
    /**
     * Health of the object. If drops to or below zero, the object is destroyed.
     */
    protected float health;
    /**
     * True if the object should be destroyed. For example it's health is not greater than zero or it went out of the game space.
     */
    protected boolean shouldDestroy = false;
    
    public Entity(SpaceWarGame game, float radius, float health) {
        this.game = game;
        this.radius = radius;
        this.health = health;
    }
    
    /**
     * Resets the entity. Used after retrieving one from a pool.
     */
    public void reset() {
        x = y = nx = ny = vx = vy = nvx = nvy = ax = ay = rot = 0;
        health = 1;
        shouldDestroy = false;
    }
    
    public SpaceWarGame getGame() {
        return game;
    }
    
    /**
     * Returns if the object can or can not collide with other objects.
     * @return whether or not the object is collidable
     */
    public boolean isCollidable() {
        return true;
    }
    
    /**
     * @return true if the object should be destroyed
     */
    public boolean shouldDestroy() {
        return shouldDestroy;
    }
    
    /**
     * @return the health of the object
     */
    public float getHealth() {
        return health;
    }
    
    /**
     * Sets the health of the object.
     * @param health
     */
    public void setHealth(float health) {
        this.health = health;
    }
    
    /**
     * Sets the position of the object. (Current and next)
     * @param x
     * @param y
     */
    public void setPosition(float x, float y) {
        this.x = nx = x;
        this.y = ny = y;
    }
    
    /**
     * @return the x coordinate of the object
     */
    public float getX() {
        return x;
    }
    
    /**
     * @return the y coordinate of the object
     */
    public float getY() {
        return y;
    }
    
    /**
     * Sets the velocity of the object along the two axes.
     * @param vx the velocity along the x axis
     * @param vy the velocity along the y axis
     */
    public void setVelocity(float vx, float vy) {
        this.vx = vx;
        this.vy = vy;
    }

    /**
     * @return the object's velocity along the x axis
     */
    public float getVX() {
        return vx;
    }
    
    /**
     * @return the object's velocity along the y axis
     */
    public float getVY() {
        return vy;
    }
    
    /**
     * Adds to the velocity of the object along the two axes.
     * @param vx the amount to add along the x axis
     * @param vy the amount to add along the y axis
     */
    public void addVelocity(float vx, float vy) {
        this.vx += vx;
        this.vy += vy;
    }
    
    /**
     * Sets the acceleration of the object along the two axes.
     * @param ax acceleration along the x axis
     * @param ay acceleration along the y axis
     */
    public void setAcceleration(float ax, float ay) {
        this.ax = ax;
        this.ay = ay;
    }
    
    /**
     * @return the acceleration of the object along the x axis
     */
    public float getAX() {
        return ax;
    }
    
    /**
     * @return the acceleration of the object along the y axis
     */
    public float getAY() {
        return ay;
    }
    
    /**
     * Sets the rotation of the object.
     * @param rot the rotation in radians
     */
    public void setRotation(float rot) {
        this.rot = rot;
    }
    
    /**
     * @return the rotation of the object in radians
     */
    public float getRotation() {
        return rot;
    }
    
    /**
     * Sets the radius of the object's bounding circle.
     * @param radius
     */
    public void setRadius(float radius) {
        this.radius = radius;
    }
    
    /**
     * @return the radius of the object's bounding circle
     */
    public float getRadius() {
        return radius;
    }
    
    /**
     * Handles inputs, ai, etc.
     * @param delta
     */
    public void preUpdate(float delta) {
        
    }
    
    /**
     * Calculates the object's new state.
     * @param delta time since the last update in seconds
     */
    public void update(float delta) {
        nx = x+vx*delta;
        ny = y+vy*delta;
        nvx = vx+ax*delta;
        nvy = vy+ay*delta;
    }
    
    /**
     * @param entity
     * @param delta time since the last update in seconds
     * @return true if the object collides with <i>entity</i>
     */
    public boolean isCollidingWith(Entity entity, float delta) {
        return isCollidable() && entity.isCollidable() && (entity.nx-nx)*(entity.nx-nx)+(entity.ny-ny)*(entity.ny-ny) <= radius*radius + entity.radius*entity.radius;
    }
    
    /**
     * Handles the collision with <i>entity</i> from this objects side. Should not make any changes to <i>entity</i>!
     * @param entity
     * @param delta time since the last update in seconds
     */
    public void handleCollision(Entity entity, float delta) {
        
    }
    
    /**
     * Changes the object's state.
     * @param delta time since the last update in seconds
     */
    public void postUpdate(float delta) {
        x = nx;
        y = ny;
        vx = nvx;
        vy = nvy;
        if(health <= 0 || x < -game.SIZE || game.SIZE*2 < x || y < -game.SIZE || game.SIZE*2 < y)
            shouldDestroy = true;
    }
    
    private Color bbColor = new Color(255, 0, 0, 127);
    
    /**
     * Renders the object.
     * @param r the renderer object to use
     * @param delta time since the last update in seconds
     */
    public void render(Renderer r, float delta) {
        if(RENDERBOUNDING) {
            r.setColor(bbColor);
            r.fillCircle((int)(getX()+getVX()*delta*0), (int)(getY()+getVY()*delta*0), (int)radius);
        }
    }
    
    /**
     * Creates a copy of the object. Used when calling <i>handleCollision(Entity entity, float delta)</i> from each direction.
     * @return a copy of the object
     */
    public Entity cloneEntity() {
        try {
            return (Entity)clone();
        } catch(CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Destroys the object.
     */
    public void destroy() {
        
    }
}
