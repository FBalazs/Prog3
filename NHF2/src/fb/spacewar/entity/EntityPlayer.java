package fb.spacewar.entity;

import java.awt.Color;
import java.awt.geom.AffineTransform;

import fb.spacewar.SpaceWarGame;
import fb.spacewar.Textures;
import fb.spacewar.display.Renderer;

public class EntityPlayer extends Entity {
    private boolean keyUp = false, keyDown = false, keyLeft = false, keyRight = false;
    private final float shotInterval = 0.66f;
    private final float bulletDamage = 2f;
    private float sinceLastShot = shotInterval;

    public EntityPlayer(SpaceWarGame game, float radius, float health) {
        super(game, radius, health);
    }

    public void setKeys(boolean keyUp, boolean keyDown, boolean keyLeft, boolean keyRight) {
        this.keyUp = keyUp;
        this.keyDown = keyDown;
        this.keyLeft = keyLeft;
        this.keyRight = keyRight;
    }

    public float getMaxVelocityHorizontal() {
        return getGame().SIZE*0.5f;
    }

    public float getMaxVelocityVertical() {
        return getGame().SIZE*0.5f;
    }

    @Override
    public void preUpdate(float delta) {
        vx = vy = 0;
        
        if(keyUp && !keyDown)
            vy = -getMaxVelocityVertical();
        else if(keyDown && !keyUp)
            vy = getMaxVelocityVertical();

        if(keyLeft && !keyRight)
            vx = -getMaxVelocityHorizontal();
        else if(keyRight && !keyLeft)
            vx = getMaxVelocityHorizontal();
        
        if(x <= radius) {
            x = radius;
            vx = Math.max(vx, 0);
        } else if(getGame().SIZE-radius <= x) {
            x = getGame().SIZE-radius;
            vx = Math.min(vx, 0);
        }
        
        if(y <= radius) {
            y = radius;
            vy = Math.max(vy, 0);
        } else if(getGame().SIZE-radius <= y) {
            y = getGame().SIZE-radius;
            vy = Math.min(vy, 0);
        }

        super.preUpdate(delta);
    }
    
    @Override
    public void handleCollision(Entity entity, float delta) {
        if(entity.getClass() == EntityBullet.class) {
            health -= entity.health;
        } else if(entity.getClass() == EntityAsteroid.class) {
            health -= entity.health;
        } else if(entity.getClass() == EntityEnemy.class) {
            health -= entity.health;
        }
    }

    @Override
    public void postUpdate(float delta) {
        super.postUpdate(delta);

        if(sinceLastShot <= shotInterval) {
            sinceLastShot += delta;
        }
        sinceLastShot += delta;
        if(shotInterval <= sinceLastShot) {
            EntityBullet bullet = getGame().getBullet();
            bullet.setHealth(bulletDamage);
            bullet.setPosition(x, Math.min(y, ny) - (radius + bullet.radius) * 1.05f);
            bullet.setVelocity(0, -getMaxVelocityVertical() * 1.5f);
            getGame().addEntity(bullet);
            sinceLastShot = 0;
        }
    }

    @Override
    public void render(Renderer r, float delta) {
        AffineTransform defTransform = r.getTransform();
        
        r.translate(x, y);
        r.rotate(rot);
        
        r.setColor(Color.WHITE);
        r.drawImage(Textures.imgPlayerShip, -radius*0.9f, -radius*0.9f, 2*radius*0.9f, 2*radius*0.9f);
        
        r.setTransform(defTransform);
        
        super.render(r, delta);
    }
}
