package fb.spacewar.entity;

import java.awt.Color;
import java.awt.geom.AffineTransform;

import fb.spacewar.SpaceWarGame;
import fb.spacewar.Textures;
import fb.spacewar.display.Renderer;

public class EntityAsteroid extends Entity {
    protected int textureIndex;
    
    public EntityAsteroid(SpaceWarGame game, float radius, float health) {
        super(game, radius, health);
        textureIndex = game.getApp().getRand().nextInt(Textures.imgAsteroid.length);
    }
    
    public EntityAsteroid(SpaceWarGame game) {
        this(game, game.SIZE/30, 10);
    }
    
    @Override
    public void handleCollision(Entity entity, float delta) {
        if(entity.getClass() == EntityPlayer.class) {
            shouldDestroy = true;
        } else if(entity.getClass() == EntityEnemy.class) {
            shouldDestroy = true;
        }
    }

    @Override
    public void render(Renderer r, float delta) {
        AffineTransform defTransform = r.getTransform();
        
        r.translate(x, y);
        r.rotate(rot);
        
        r.setColor(Color.WHITE);
        r.drawImage(Textures.imgAsteroid[textureIndex], -radius, -radius, 2*radius, 2*radius);
        
        r.setTransform(defTransform);
        
        super.render(r, delta);
    }
    
    @Override
    public void destroy() {
        super.destroy();
        getGame().freeAsteroid(this);
    }
}
