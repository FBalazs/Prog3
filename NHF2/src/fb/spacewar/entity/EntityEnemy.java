package fb.spacewar.entity;

import java.awt.Color;
import java.awt.geom.AffineTransform;

import fb.spacewar.SpaceWarGame;
import fb.spacewar.Textures;
import fb.spacewar.display.Renderer;

public class EntityEnemy extends Entity {
    private float shotInterval = 0.66f;
    private final float bulletDamage = 2f;
    private float sinceLastShot = shotInterval;
    private Entity avoidFrom = null;

    public EntityEnemy(SpaceWarGame game, float radius, float health) {
        super(game, radius, health);
    }

    public float getMaxVelocityHorizontal() {
        return getGame().SIZE*0.33f;
    }

    public float getMaxVelocityVertical() {
        return getGame().SIZE*0.33f;
    }

    @Override
    public void preUpdate(float delta) {
        if(avoidFrom != null && (avoidFrom.shouldDestroy || (radius + avoidFrom.radius)*(radius + avoidFrom.radius)*2f*2f < (avoidFrom.x-x)*(avoidFrom.x-x)+(avoidFrom.y-y)*(avoidFrom.y-y)))
            avoidFrom = null;
        
        for(Entity entity : getGame().getEntities()) {
            if (!entity.isCollidingWith(this, delta) && (entity.x-x)*(entity.x-x)+(entity.y-y)*(entity.y-y) < (radius + entity.radius)*(radius + entity.radius)*1.5f*1.5f)
                if (avoidFrom == null || Math.sqrt((entity.x-x)*(entity.x-x)+(entity.y-y)*(entity.y-y)) - entity.radius < Math.sqrt((avoidFrom.x-x)*(avoidFrom.x-x)+(avoidFrom.y-y)*(avoidFrom.y-y)) - avoidFrom.radius)
                    avoidFrom = entity;
        }
        
        if(avoidFrom == null) {
            vx = (getGame().getPlayer().x-x)*5f;
            if(Math.abs(vx) > getMaxVelocityHorizontal())
                vx = getMaxVelocityHorizontal()*Math.signum(vx);
        } else {
            vx = Math.signum(x-avoidFrom.x)*getMaxVelocityHorizontal();
        }
        vy = getMaxVelocityVertical()/10;
        
        super.preUpdate(delta);
    }
    
    @Override
    public void handleCollision(Entity entity, float delta) {
        if(entity.getClass() == EntityBullet.class) {
            health -= entity.health;
        } else if(entity.getClass() == EntityAsteroid.class) {
            health -= entity.health;
        } else if(entity.getClass() == EntityPlayer.class) {
            shouldDestroy = true;
        }
    }

    @Override
    public void postUpdate(float delta) {
        super.postUpdate(delta);
        
        if(sinceLastShot <= shotInterval) {
            sinceLastShot += delta;
        }
        sinceLastShot += delta;
        if(shotInterval <= sinceLastShot) {
            EntityBullet bullet = getGame().getBullet();
            bullet.setHealth(bulletDamage);
            bullet.setPosition(x, Math.max(y, ny) + (radius + bullet.radius) * 1.05f);
            bullet.setVelocity(0, getMaxVelocityVertical() * 1.5f);
            bullet.setRotation((float)Math.PI);
            getGame().addEntity(bullet);
            sinceLastShot = 0;
        }
    }

    @Override
    public void render(Renderer r, float delta) {
        AffineTransform defTransform = r.getTransform();
        
        r.translate(x, y);
        r.rotate(rot);
        
        r.setColor(Color.WHITE);
        r.drawImage(Textures.imgEnemyShip, -radius*0.9f, -radius*0.9f, 2*radius*0.9f, 2*radius*0.9f);
        
        r.setTransform(defTransform);
        
        super.render(r, delta);
    }
}
