package fb.spacewar.entity;

import java.awt.Color;
import java.awt.geom.AffineTransform;

import fb.spacewar.SpaceWarGame;
import fb.spacewar.Textures;
import fb.spacewar.display.Renderer;

public class EntityBullet extends Entity {
    public EntityBullet(SpaceWarGame game, float damage) {
        super(game, game.SIZE/100, damage);
    }
    
    public EntityBullet(SpaceWarGame game) {
        this(game, 10);
    }
    
    @Override
    public void handleCollision(Entity entity, float delta) {
        if(entity.getClass() == EntityAsteroid.class || entity.getClass() == EntityPlayer.class || entity.getClass() == EntityEnemy.class)
            shouldDestroy = true;
    }
    
    @Override
    public void render(Renderer r, float delta) {
        AffineTransform defTransform = r.getTransform();
        
        r.translate(x, y);
        r.rotate(rot);
        
        r.setColor(Color.WHITE);
        r.drawImage(Textures.imgBullet, -radius, -radius, 2*radius, 2*radius);
        
        r.setTransform(defTransform);
        
        super.render(r, delta);
    }
    
    @Override
    public void destroy() {
        super.destroy();
        getGame().freeBullet(this);
    }
}
