package fb.spacewar;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class Textures {
    public static BufferedImage imgPlayerShip,
                                imgEnemyShip,
                                imgBullet;
    
    public static BufferedImage[] imgAsteroid;
    
    private static BufferedImage loadIMG(String name) {
        BufferedImage ret;
        try {
            ret = ImageIO.read(new File("res/"+name+".png"));
        } catch(Exception e) {
            e.printStackTrace();
            ret = new BufferedImage(2, 2, BufferedImage.TYPE_INT_ARGB);
            ret.setRGB(0, 0, Color.BLACK.getRGB());
            ret.setRGB(1, 0, Color.PINK.getRGB());
            ret.setRGB(0, 1, Color.PINK.getRGB());
            ret.setRGB(1, 1, Color.BLACK.getRGB());
        }
        return ret;
    }
    
    public static void load() {
        imgPlayerShip = loadIMG("player_ship_2");
        imgEnemyShip = loadIMG("enemy_ship_2");
        imgBullet = loadIMG("fireshot");
        
        imgAsteroid = new BufferedImage[5];
        for(int i = 0; i < imgAsteroid.length; ++i)
            imgAsteroid[i] = loadIMG("asteroid_"+(i+1));
    }
}
