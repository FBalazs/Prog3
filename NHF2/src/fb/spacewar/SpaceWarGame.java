package fb.spacewar;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.util.Iterator;
import java.util.LinkedList;

import fb.spacewar.display.Renderer;
import fb.spacewar.entity.Entity;
import fb.spacewar.entity.EntityAsteroid;
import fb.spacewar.entity.EntityBullet;
import fb.spacewar.entity.EntityEnemy;
import fb.spacewar.entity.EntityPlayer;
import fb.spacewar.screen.ScreenEnd;
import fb.util.Align;
import fb.util.GameLoopThread;
import fb.util.GameLoopThread.GameLoopCallback;
import fb.util.Time;
import fb.util.WeakPool;

/**
 * Game class for holding the current game session.
 */
public class SpaceWarGame extends GameLoopCallback {
    /**
     * Reference for the application that holds this game object.
     */
    private SpaceWarApp app;

    /**
     * The canvas to draw the game on.
     */
    private Canvas canvas;
    /**
     * The thread that calls the specified functions of the object.
     */
    private GameLoopThread thread = new GameLoopThread(this);

    /**
     * The Renderer object to draw with.
     */
    private Renderer r = new Renderer();

    /**
     * The size of the coordinate system. Top left corner is 0,0, bottom right is SIZE,SIZE.
     */
    public final float SIZE = 1000;
    
    /**
     * The list that holds all the entities in the game.
     */
    private LinkedList<Entity> entities = new LinkedList<>();
    /**
     * The entity that is controlled by the user.
     */
    private EntityPlayer player = null;
    private LinkedList<Entity> entitiesToAdd = new LinkedList<>();
    
    /**
     * A Pool for the bullets.
     */
    private WeakPool<EntityBullet> bulletPool;
    /**
     * A Pool for the asteroids.
     */
    private WeakPool<EntityAsteroid> asteroidPool;
    
    /**
     * The time until the next asteroid spawns (in seconds).
     */
    private float nextAsteroidTime = 0;
    private float maxAsteroidTime = 3;
    
    private float nextEnemyTime = 5;
    private float maxEnemyTime = 10;
    
    /**
     * The time when the game started.
     */
    private long startTime;
    
    private final float maxPlayerHealth = 100;
    
    /**
     * Creates a game object with a reference for the application.
     * @param app
     */
    public SpaceWarGame(SpaceWarApp app) {
        this.app = app;
        thread.setRUPS(100);
        thread.setRRPS(200);
    }

    /**
     * @return the application
     */
    public SpaceWarApp getApp() {
        return app;
    }

    /**
     * @return the canvas
     */
    public Canvas getCanvas() {
        return canvas;
    }

    /**
     * @return sets the canvas to draw the game to
     */
    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }
    
    /**
     * @return the renderer
     */
    public Renderer getRenderer() {
        return r;
    }
    
    /**
     * @return the list of current entities
     */
    public LinkedList<Entity> getEntities() {
        return entities;
    }
    
    /**
     * Adds an entity to the addition queue. The entity will be placed in the current entity list at the end of the update sequence.
     * @param entity
     */
    public void addEntity(Entity entity) {
        entitiesToAdd.add(entity);
    }

    /**
     * Sets the player.
     * @param player
     */
    public void setPlayer(EntityPlayer player) {
        this.player = player;
    }

    /**
     * @return the player
     */
    public EntityPlayer getPlayer() {
        return player;
    }
    
    /**
     * Starts the game thread.
     */
    public void start() {
        startTime = Time.getTime();
        thread.start();
    }
    
    /**
     * Stops the game thread.
     */
    public void stop() {
        thread.stop();
    }
    
    /**
     * Reuses a bullet from the destroyed ones or creates a new one.
     * @return the bullet
     */
    public EntityBullet getBullet() {
        EntityBullet ret = bulletPool.get();
        ret.reset();
        return ret;
    }
    
    /**
     * Adds the destroyed bullet to the bullet pool.
     * @param bullet
     */
    public void freeBullet(EntityBullet bullet) {
        bulletPool.add(bullet);
    }
    
    /**
     * Reuses an asteroid from the destroyed ones or creates a new one.
     * @return the asteroid
     */
    public EntityAsteroid getAsteroid() {
        EntityAsteroid ret = asteroidPool.get();
        ret.reset();
        return ret;
    }
    
    /**
     * Adds the destroyed asteroid to the asteroid pool.
     * @param asteroid
     */
    public void freeAsteroid(EntityAsteroid asteroid) {
        asteroidPool.add(asteroid);
    }

    /**
     * Initializes the game. Creates the player and places him/her in the world.
     */
    @Override
    public void init() {
        try {
            bulletPool = new WeakPool<>(EntityBullet.class.getConstructor(SpaceWarGame.class), this);
            asteroidPool = new WeakPool<>(EntityAsteroid.class.getConstructor(SpaceWarGame.class), this);
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        player = new EntityPlayer(this, SIZE/25, maxPlayerHealth);
        entities.add(player);
        
        player.setPosition(SIZE/2, SIZE*3/4);
    }
    
    /**
     * Updates the game in the following order:
     * 1. spawns new asteroids if needed
     * 2. spawns new enemies if needed
     * 3. calls every entity's preUpdate method
     * 4. calls every entity's update method
     * 5. checks every entity pair for collision, and calls their handleCollision method if needed
     * 6. calls every entity's postUpdate method and destroys those should be destroyed
     * 7. adds new entities
     * 8. checks if the player is dead
     * @param delta the time since the last update in seconds
     */
    @Override
    public void update(float delta) {
        nextAsteroidTime -= delta;
        while(nextAsteroidTime <= 0) {
            EntityAsteroid asteroid = getAsteroid();
            asteroid.setRadius(app.getRand().nextFloat()*SIZE/25+SIZE/50);
            asteroid.setHealth(asteroid.getRadius()*asteroid.getRadius()/SIZE/SIZE*10000);
            asteroid.setPosition(app.getRand().nextFloat()*SIZE, -SIZE/4);
            asteroid.setVelocity(0, SIZE/16+SIZE/8*app.getRand().nextFloat());
            asteroid.setRotation(app.getRand().nextFloat()*(float)Math.PI*2);
            addEntity(asteroid);
            
            nextAsteroidTime += app.getRand().nextFloat()*maxAsteroidTime;
            if(0.2f < maxAsteroidTime)
                maxAsteroidTime -= 0.01f;
        }
        
        nextEnemyTime -= delta;
        while(nextEnemyTime <= 0) {
            EntityEnemy enemy = new EntityEnemy(this, player.getRadius(), maxPlayerHealth/10);
            enemy.setPosition(app.getRand().nextFloat()*SIZE, -player.getRadius());
            enemy.setRotation((float)Math.PI);
            addEntity(enemy);
            
            nextEnemyTime += app.getRand().nextFloat()*maxEnemyTime;
            if(2f < maxEnemyTime)
                maxEnemyTime -= 0.5f;
        }
        
        for(Entity entity : entities)
            entity.preUpdate(delta);
        for(Entity entity : entities)
            entity.update(delta);
        Entity entity1, entity2;
        for(Iterator<Entity> it1 = entities.iterator(); it1.hasNext();) {
            entity1 = it1.next();
            Iterator<Entity> it2 = entities.iterator();
            while(!entity1.equals(entity2 = it2.next()));
            while(it2.hasNext()) {
                entity2 = it2.next();
                if(entity1.isCollidingWith(entity2, delta)) {
                    Entity tempEntity = entity1.cloneEntity();
                    entity1.handleCollision(entity2, delta);
                    entity2.handleCollision(tempEntity, delta);
                }
            }
        }
        for(Iterator<Entity> it = entities.iterator(); it.hasNext();) {
            entity1 = it.next();
            entity1.postUpdate(delta);
            if(entity1.shouldDestroy()) {
                entity1.destroy();
                it.remove();
            }
        }
        entities.addAll(entitiesToAdd);
        entitiesToAdd.clear();
        
        if(player.getHealth() <= 0) {
            app.setScreen(new ScreenEnd(app, null, thread.getTickStartTime()-startTime));
        }
    }

    /**
     * Renders(draws) the game.
     * @param delta the time since the last update in seconds
     */
    @Override
    public void render(float delta) {
        if(canvas == null)
            return;
        
        r.begin(canvas);
        AffineTransform defTransform = r.getTransform();
        if(canvas.getHeight() < canvas.getWidth()) {
            r.translate((canvas.getWidth()-canvas.getHeight())/2f, 0);
            r.scale(canvas.getHeight()/SIZE, canvas.getHeight()/SIZE);
        } else {
            r.translate(0, (canvas.getHeight()-canvas.getWidth())/2f);
            r.scale(canvas.getWidth()/SIZE, canvas.getWidth()/SIZE);
        }
        
        r.setColor(Color.BLACK);
        r.fillRect(0, 0, (int)SIZE, (int)SIZE);
        
        for(Entity entity : entities)
            entity.render(r, delta);
        
        
        
        r.setTransform(defTransform);
        r.setColor(Color.DARK_GRAY);
        if(canvas.getHeight() < canvas.getWidth()) {
            r.fillRect(0, 0, (int)Math.ceil((canvas.getWidth()-canvas.getHeight())/2f), canvas.getHeight());
            r.fillRect((int)Math.floor(canvas.getWidth()-(canvas.getWidth()-canvas.getHeight())/2f), 0, (int)Math.ceil((canvas.getWidth()-canvas.getHeight())/2f), canvas.getHeight());
        } else {
            r.fillRect(0, 0, canvas.getWidth(), (int)Math.ceil((canvas.getHeight()-canvas.getWidth())/2f));
            r.fillRect(0, (int)Math.floor(canvas.getHeight()-(canvas.getHeight()-canvas.getWidth())/2f), canvas.getWidth(), (int)Math.ceil((canvas.getHeight()-canvas.getWidth())/2f));
        }
        
        r.setColor(Color.WHITE);
        r.drawString("UPS: "+thread.getUPS(), 0, 0, Align.LEFT, Align.TOP);
        r.drawString("RPS: "+thread.getRPS(), 0, 16, Align.LEFT, Align.TOP);
        r.drawString("Entities: "+entities.size(), 0, 32, Align.LEFT, Align.TOP);
        
        r.setColor(Color.DARK_GRAY);
        r.fillRect(0, canvas.getHeight()-20, 100, 20);
        r.setColor(Color.GREEN);
        r.fillRect(0, canvas.getHeight()-20, (int)(100*player.getHealth()/maxPlayerHealth), 20);
        r.setColor(Color.LIGHT_GRAY);
        r.drawRect(0, canvas.getHeight()-20, 100, 20);
        
        r.setColor(Color.WHITE);
        long min = (thread.getTickStartTime()-startTime)/Time.ONE_SECOND/60;
        int sec = (int)(((thread.getTickStartTime()-startTime)/Time.ONE_SECOND)%60);
        r.drawString(min+(sec < 10 ? ":0" : ":")+sec, 0, canvas.getHeight()-20, Align.LEFT, Align.BOTTOM);
        
        r.end();
    }
}
