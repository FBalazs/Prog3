package fb.spacewar.screen;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import fb.spacewar.SpaceWarApp;

/**
 * Displays the 10 best games.
 */
public class ScreenRecords extends Screen {
    private JTable table;
    private JButton btnBack;
    
    public ScreenRecords(SpaceWarApp app, Screen parent) {
        super(app, parent);
    }
    
    /**
     * @wbp.parser.entryPoint
     */
    @Override
    public JPanel createPanel() {
        JPanel panel = new JPanel();
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_panel.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);
        
        table = new JTable();
        GridBagConstraints gbc_table = new GridBagConstraints();
        gbc_table.insets = new Insets(0, 0, 5, 5);
        gbc_table.fill = GridBagConstraints.BOTH;
        gbc_table.gridx = 1;
        gbc_table.gridy = 1;
        panel.add(table, gbc_table);
        
        btnBack = new JButton("Back");
        GridBagConstraints gbc_btnBack = new GridBagConstraints();
        gbc_btnBack.insets = new Insets(0, 0, 5, 5);
        gbc_btnBack.gridx = 1;
        gbc_btnBack.gridy = 2;
        panel.add(btnBack, gbc_btnBack);
        
        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getApp().setScreen(getParent());
            }
        });
        
        return panel;
    }
    
    @Override
    public void open() {
        try {
            File f = new File("records.txt");
            DefaultTableModel model = new DefaultTableModel(10, 1) {
                private static final long serialVersionUID = 1L;

                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            if(f.exists()) {
                Scanner scanner = new Scanner(f);
                for(int i = 0; i < 10; ++i) {
                    long min = scanner.nextLong();
                    int sec = (int)(min%60);
                    min = min/60;
                    model.setValueAt(min+(sec < 10 ? ":0" : ":")+sec, i, 0);
                }
                scanner.close();
            } else {
                for(int i = 0; i < 10; ++i)
                    model.setValueAt("0:00", i, 0);
            }
            table.setModel(model);
        } catch(Exception e) {
            getApp().close(e);
        }
    }
}
