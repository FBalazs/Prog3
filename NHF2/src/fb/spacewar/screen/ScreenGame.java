package fb.spacewar.screen;

import java.awt.Canvas;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

import fb.spacewar.SpaceWarApp;
import fb.spacewar.SpaceWarGame;
import fb.spacewar.entity.EntityPlayer;

/**
 * Screen for the current game session.
 */
public class ScreenGame extends Screen implements KeyListener {
    private Canvas canvas;
    
    public ScreenGame(SpaceWarApp app, Screen parent) {
        super(app, parent);
    }
    
    private SpaceWarGame getGame() {
        return getApp().getGame();
    }
    
    private EntityPlayer getPlayer() {
        return getApp().getGame().getPlayer();
    }

    /**
     * @wbp.parser.entryPoint
     */
    @Override
    public JPanel createPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 1));
        
        canvas = new Canvas();
        canvas.addKeyListener(this);
        panel.add(canvas);
        
        return panel;
    }
    
    @Override
    public void open() {
        getGame().setCanvas(canvas);
        getGame().start();
    }
    
    @Override
    public void close() {
        getGame().stop();
        getGame().setCanvas(null);
    }
    
    private boolean keyUp = false, keyDown = false, keyLeft = false, keyRight = false;
    
    private void updatePlayerInput() {
        getPlayer().setKeys(keyUp, keyDown, keyLeft, keyRight);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                keyUp = true;
                updatePlayerInput();
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                keyDown = true;
                updatePlayerInput();
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                keyLeft = true;
                updatePlayerInput();
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                keyRight = true;
                updatePlayerInput();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch(e.getKeyCode()) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                keyUp = false;
                updatePlayerInput();
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                keyDown = false;
                updatePlayerInput();
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                keyLeft = false;
                updatePlayerInput();
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                keyRight = false;
                updatePlayerInput();
                break;
        }
    }
    
    @Override
    public void keyTyped(KeyEvent e) {}
}
