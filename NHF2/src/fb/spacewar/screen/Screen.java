package fb.spacewar.screen;

import javax.swing.JPanel;

import fb.spacewar.SpaceWarApp;

/**
 * Represents a GUI screen.
 */
public abstract class Screen {
    /**
     * The application object.
     */
    private SpaceWarApp app;
    /**
     * The parent screen.
     */
    private Screen parent;
    /**
     * The panel of the screen.
     */
    private JPanel panel;
    
    public Screen(SpaceWarApp app, Screen parent) {
        this.app = app;
        this.parent = parent;
        panel = createPanel();
    }
    
    /**
     * @return the application object
     */
    public SpaceWarApp getApp() {
        return app;
    }
    
    /**
     * @return the parent screen
     */
    public Screen getParent() {
        return parent;
    }
    
    /**
     * @return the panel of the screen
     */
    public JPanel getPanel() {
        return panel;
    }
    
    /**
     * Creates the panel of the screen.
     * @return the panel of the screen
     */
    public abstract JPanel createPanel();
    
    /**
     * Called when the screen opens.
     */
    public void open() {
        
    }
    
    /**
     * Called when the screen closes.
     */
    public void close() {
        
    }
}
