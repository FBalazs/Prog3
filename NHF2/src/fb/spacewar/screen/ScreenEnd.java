package fb.spacewar.screen;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fb.spacewar.SpaceWarApp;
import fb.util.Time;

/**
 * Displays the score of the player at the end of the game.
 */
public class ScreenEnd extends Screen {
    private long time;
    private JLabel lblText;
    private JButton btnOk;
    
    public ScreenEnd(SpaceWarApp app, Screen parent, long time) {
        super(app, parent);
        this.time = time/Time.ONE_SECOND;
    }
    
    /**
     * @wbp.parser.entryPoint
     */
    @Override
    public JPanel createPanel() {
        JPanel panel = new JPanel();
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_panel.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);
        
        lblText = new JLabel("!placeholder!");
        GridBagConstraints gbc_lblText = new GridBagConstraints();
        gbc_lblText.insets = new Insets(0, 0, 5, 5);
        gbc_lblText.gridx = 1;
        gbc_lblText.gridy = 1;
        panel.add(lblText, gbc_lblText);
        
        btnOk = new JButton("OK");
        GridBagConstraints gbc_btnOk = new GridBagConstraints();
        gbc_btnOk.insets = new Insets(0, 0, 5, 5);
        gbc_btnOk.gridx = 1;
        gbc_btnOk.gridy = 2;
        panel.add(btnOk, gbc_btnOk);
        
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getApp().setScreen(new ScreenMainMenu(getApp(), null));
            }
        });
        
        return panel;
    }
    
    @Override
    public void open() {
        long min = time/60;
        int sec = (int)(time%60);
        lblText.setText("Your time is: "+min+(sec < 10 ? ":0" : ":")+sec+" Congratulations!");
        
        try {
            long[] records = new long[10];
            
            File f = new File("records.txt");
            if(f.exists()) {
                Scanner scanner = new Scanner(f);
                for(int i = 0; i < records.length; ++i)
                    records[i] = scanner.nextLong();
                scanner.close();
            } else
                for(int i = 0; i < records.length; ++i)
                    records[i] = 0;
            
            if(records[records.length-1] < time) {
                int i = records.length-2;
                while(0 <= i && records[i] < time)
                    --i;
                ++i;
                for(int j = records.length-1; i < j; --j)
                    records[j] = records[j-1];
                records[i] = time;
                
                PrintWriter pw = new PrintWriter(f);
                for(int j = 0; j < 10; ++j)
                    pw.println(records[j]);
                pw.close();
            }
        } catch(Exception e) {
            getApp().close(e);
        }
    }
}
