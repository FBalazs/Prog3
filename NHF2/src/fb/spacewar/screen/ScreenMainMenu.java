package fb.spacewar.screen;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import fb.spacewar.SpaceWarApp;
import fb.spacewar.SpaceWarGame;

/**
 * Displays the main menu.
 */
public class ScreenMainMenu extends Screen {
    private JButton btnStart;
    private JButton btnRecords;
    private JButton btnExit;

    public ScreenMainMenu(SpaceWarApp app, Screen parent) {
        super(app, parent);
    }
    
    /**
     * @wbp.parser.entryPoint
     */
    @Override
    public JPanel createPanel() {
        JPanel panel = new JPanel();
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
        gbl_panel.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);
        
        btnStart = new JButton("Start Game");
        GridBagConstraints gbc_btnJoin = new GridBagConstraints();
        gbc_btnJoin.fill = GridBagConstraints.HORIZONTAL;
        gbc_btnJoin.insets = new Insets(0, 0, 5, 5);
        gbc_btnJoin.gridx = 1;
        gbc_btnJoin.gridy = 1;
        panel.add(btnStart, gbc_btnJoin);
        
        btnRecords = new JButton("Records");
        GridBagConstraints gbc_btnRecords = new GridBagConstraints();
        gbc_btnRecords.fill = GridBagConstraints.HORIZONTAL;
        gbc_btnRecords.insets = new Insets(0, 0, 5, 5);
        gbc_btnRecords.gridx = 1;
        gbc_btnRecords.gridy = 2;
        panel.add(btnRecords, gbc_btnRecords);
        
        btnExit = new JButton("Exit");
        GridBagConstraints gbc_btnExit = new GridBagConstraints();
        gbc_btnExit.fill = GridBagConstraints.HORIZONTAL;
        gbc_btnExit.insets = new Insets(0, 0, 5, 5);
        gbc_btnExit.gridx = 1;
        gbc_btnExit.gridy = 3;
        panel.add(btnExit, gbc_btnExit);
        
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getApp().setGame(new SpaceWarGame(getApp()));
                getApp().setScreen(new ScreenGame(getApp(), ScreenMainMenu.this));
            }
        });
        
        btnRecords.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getApp().setScreen(new ScreenRecords(getApp(), ScreenMainMenu.this));
            }
        });
        
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getApp().close();
            }
        });
        
        return panel;
    }
}
