package fb.spacewar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.Random;

import javax.swing.JOptionPane;

import fb.spacewar.display.Display;
import fb.spacewar.display.DisplayListener;
import fb.spacewar.screen.Screen;
import fb.spacewar.screen.ScreenMainMenu;

public class SpaceWarApp {
    public static void main(String[] args) {
        new SpaceWarApp();
    }

    public static final String PROPERTY_DISPLAY_X = "display_x", PROPERTY_DISPLAY_Y = "display_y",
            PROPERTY_DISPLAY_WIDTH = "display_width", PROPERTY_DISPLAY_HEIGHT = "display_height";

    /**
     * The display of the application.
     */
    private Display display;

    /**
     * Configuration file.
     */
    private File configFile = new File("config.xml");
    /**
     * Properties object.
     */
    private Properties props = new Properties();

    /**
     * Reference for the current game session.
     */
    private SpaceWarGame game = null;
    
    /**
     * Random object for generating random numbers.
     */
    private Random rand = new Random();

    /**
     * Constructor. Creates the object and initializes it.
     * 1. Loads the configuration file.
     * 2. Loads the textures.
     * 3. Initializes the display.
     * 4. Opens the main menu.
     */
    public SpaceWarApp() {
        try {
            loadConfig();
    
            Textures.load();
            
            display = new Display();
            display.setTitle("Space War");
            display.setLocation(Integer.parseInt(getProperties().getProperty(PROPERTY_DISPLAY_X)),
                    Integer.parseInt(getProperties().getProperty(PROPERTY_DISPLAY_Y)));
            display.setSize(Integer.parseInt(getProperties().getProperty(PROPERTY_DISPLAY_WIDTH)),
                    Integer.parseInt(getProperties().getProperty(PROPERTY_DISPLAY_HEIGHT)));
            display.setListener(new DisplayListener() {
                @Override
                public void onClosing(Display display) {
                    close();
                }
            });
    
            display.setScreen(new ScreenMainMenu(this, null));
        } catch(Exception e) {
            close(e);
        }
    }

    /**
     * @return the display
     */
    public Display getDisplay() {
        return display;
    }

    /**
     * @return the properties
     */
    public Properties getProperties() {
        return props;
    }

    /**
     * Sets the current screen.
     * @param screen the screen to display
     */
    public void setScreen(Screen screen) {
        display.setScreen(screen);
    }

    /**
     * @return the current screen
     */
    public Screen getScreen() {
        return display.getScreen();
    }

    /**
     * Sets the current game session.
     * @param game the game session
     */
    public void setGame(SpaceWarGame game) {
        this.game = game;
    }

    /**
     * @return the current game session
     */
    public SpaceWarGame getGame() {
        return game;
    }
    
    /**
     * @return the random generator
     */
    public Random getRand() {
        return rand;
    }

    /**
     * Loads the configurations.
     */
    public void loadConfig() {
        props.setProperty(PROPERTY_DISPLAY_X, "-1");
        props.setProperty(PROPERTY_DISPLAY_Y, "-1");
        props.setProperty(PROPERTY_DISPLAY_WIDTH, "800");
        props.setProperty(PROPERTY_DISPLAY_HEIGHT, "600");

        if(configFile.exists()) {
            try {
                FileInputStream in = new FileInputStream(configFile);
                props.loadFromXML(in);
                in.close();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Saves the configurations.
     */
    public void saveConfig() {
        props.setProperty(PROPERTY_DISPLAY_X, new Integer(display.getX()).toString());
        props.setProperty(PROPERTY_DISPLAY_Y, new Integer(display.getY()).toString());
        props.setProperty(PROPERTY_DISPLAY_WIDTH, new Integer(display.getWidth()).toString());
        props.setProperty(PROPERTY_DISPLAY_HEIGHT, new Integer(display.getHeight()).toString());
        try {
            FileOutputStream out = new FileOutputStream(configFile);
            props.storeToXML(out, null);
            out.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes the application.
     * @param e the exception that caused the application to close or null if there wasn't one
     */
    public void close(Exception e) {
        if(e != null) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e, "Space War Error", JOptionPane.ERROR_MESSAGE);
        }
        try {
            saveConfig();
            setScreen(null);
        } catch(Exception e1) {
            e1.printStackTrace();
        }
        System.exit(0);
    }

    /**
     * Closes the application.
     */
    public void close() {
        close(null);
    }
}
