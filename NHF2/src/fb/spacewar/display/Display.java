package fb.spacewar.display;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import fb.spacewar.screen.Screen;

/**
 * The window for the application.
 */
public class Display {
    /*
     * The window.
     */
    private JFrame frame = new JFrame();
    /*
     * The current screen.
     */
    private Screen screen = null;
    
    /**
     * Listener for the object.
     */
    private DisplayListener listener = null;
    
    public Display() {
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(listener != null)
                    listener.onClosing(Display.this);
            }
        });
        frame.setVisible(true);
    }
    
    /**
     * Sets the current screen. Closes the previous one and opens the new one.
     * @param screen
     */
    public void setScreen(Screen screen) {
        if(this.screen != null) {
            this.screen.close();
            frame.remove(this.screen.getPanel());
        }
        this.screen = screen;
        if(screen != null) {
            frame.add(screen.getPanel());
            screen.open();
        }
        frame.setVisible(true);
        frame.repaint();
    }
    
    /**
     * @return the current screen
     */
    public Screen getScreen() {
        return screen;
    }
    
    /**
     * Sets the window's title.
     * @param title
     */
    public void setTitle(String title) {
        frame.setTitle(title);
    }
    
    /**
     * @return the windows's title
     */
    public String getTitle() {
        return frame.getTitle();
    }
    
    /**
     * @return the window's x coordinate on the monitor
     */
    public int getX() {
        return frame.getLocationOnScreen().x;
    }
    
    /**
     * @return the window's y coordinate on the monitor
     */
    public int getY() {
        return frame.getLocationOnScreen().y;
    }
    
    /**
     * @return the window's width
     */
    public int getWidth() {
        return frame.getWidth();
    }
    
    /**
     * @return the window's height
     */
    public int getHeight() {
        return frame.getHeight();
    }
    
    /**
     * Sets the window's location on the monitor.
     * @param x
     * @param y
     */
    public void setLocation(int x, int y) {
        frame.setLocation(x, y);
    }
    
    /**
     * Sets the window's width and height.
     * @param width
     * @param height
     */
    public void setSize(int width, int height) {
        frame.setSize(width, height);
    }
    
    /**
     * Sets a listener for this window.
     * @param listener
     */
    public void setListener(DisplayListener listener) {
        this.listener = listener;
    }
}
