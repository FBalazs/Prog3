package fb.spacewar.display;

public interface DisplayListener {
    public void onClosing(Display display);
}
