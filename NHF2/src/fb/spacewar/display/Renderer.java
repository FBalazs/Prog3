package fb.spacewar.display;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import fb.util.Align;

/**
 * Renderer object for drawing.
 */
public class Renderer {
    /**
     * The current canvas.
     */
    private Canvas canvas = null;
    /**
     * The current graphics object.
     */
    private Graphics2D g2 = null;
    
    /**
     * @return the current canvas
     */
    public Canvas getCanvas() {
        return canvas;
    }
    
    /**
     * @return the current graphics object
     */
    public Graphics2D getGraphics() {
        return g2;
    }
    
    /**
     * @return the width of the current canvas
     */
    public int getWidth() {
        return canvas.getWidth();
    }
    
    /**
     * @return the height of the current canvas
     */
    public int getHeight() {
        return canvas.getHeight();
    }
    
    /**
     * Starts rendering on the given canvas.
     * @param canvas
     */
    public void begin(Canvas canvas) {
        if(canvas != null)
            end();
        this.canvas = canvas;
        if(canvas.getBufferStrategy() == null)
            canvas.createBufferStrategy(2);
        g2 = (Graphics2D)canvas.getBufferStrategy().getDrawGraphics();
    }
    
    /**
     * Ends rendering on the current canvas. Flips buffers.
     */
    public void end() {
        if(canvas == null)
            return;
        canvas.getBufferStrategy().show();
        g2.dispose();
        canvas = null;
        g2 = null;
    }
    
    public void rotate(double rad) {
        g2.rotate(rad);
    }
    
    public void rotate(double rad, double x, double y) {
        g2.rotate(rad, x, y);
    }
    
    public void scale(double sx, double sy) {
        g2.scale(sx, sy);
    }
    
    public void shear(double shx, double shy) {
        g2.shear(shx, shy);
    }
    
    public void translate(double tx, double ty) {
        g2.translate(tx, ty);
    }
    
    public void setColor(Color color) {
        g2.setColor(color);
    }
    
    public void fill() {
        fillRect(0, 0, getWidth(), getHeight());
    }
    
    public void drawRect(int x, int y, int width, int height) {
        g2.drawRect(x, y, width, height);
    }
    
    public void fillRect(int x, int y, int width, int height) {
        g2.fillRect(x, y, width, height);
    }
    
    public void drawCircle(int x, int y, int r) {
        g2.drawOval(x-r, y-r, 2*r, 2*r);
    }
    
    public void fillCircle(int x, int y, int r) {
        g2.fillOval(x-r, y-r, 2*r, 2*r);
    }
    
    public void drawString(String str, float x, float y, int halign, int valign) {
        switch(halign) {
            case Align.LEFT:
                break;
            case Align.CENTER:
                x -= g2.getFontMetrics().stringWidth(str)/2f;
                break;
            case Align.RIGHT:
                x -= g2.getFontMetrics().stringWidth(str);
                break;
        }
        
        switch(valign) {
            case Align.TOP:
                y += g2.getFontMetrics().getAscent();
                break;
            case Align.CENTER:
                y += (g2.getFontMetrics().getAscent()-g2.getFontMetrics().getDescent())/2f;
                break;
            case Align.BOTTOM:
                y -= g2.getFontMetrics().getDescent();
                break;
        }
        
        g2.drawString(str, x, y);
    }
    
    public void drawImage(BufferedImage img, float x, float y, float width, float height) {
        AffineTransform defTransfrom = g2.getTransform();
        g2.translate(x, y);
        g2.scale(width/img.getWidth(), height/img.getHeight());
        g2.drawImage(img, null, null);
        g2.setTransform(defTransfrom);
    }
    
    public AffineTransform getTransform() {
        return g2.getTransform();
    }
    
    public void setTransform(AffineTransform transform) {
        g2.setTransform(transform);
    }
}
