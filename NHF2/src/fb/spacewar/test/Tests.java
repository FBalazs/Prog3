package fb.spacewar.test;

import static org.junit.Assert.*;

import org.junit.Test;

import fb.spacewar.Textures;
import fb.spacewar.entity.Entity;
import fb.util.GameLoopThread;
import fb.util.GameLoopThread.GameLoopCallback;
import fb.util.GameLoopThread.IGameLoopCallback;
import fb.util.Pool;

public class Tests {
    private int runningThreads;
    private int sleep = 50;

    private IGameLoopCallback myGameLoopCallback = new GameLoopCallback() {
        @Override
        public void init() {
            ++runningThreads;
        }

        @Override
        public void dispose() {
            --runningThreads;
        }
    };

    @Test
    public void testGameLoopThread() {
        try {
            runningThreads = 0;
            GameLoopThread thread1 = new GameLoopThread(myGameLoopCallback);
            GameLoopThread thread2 = new GameLoopThread(myGameLoopCallback);
            thread1.start();

            assertEquals(0, runningThreads);

            Thread.sleep(sleep);

            assertEquals(1, runningThreads);

            thread1.start();
            Thread.sleep(sleep);

            assertEquals(1, runningThreads);

            thread2.start();
            Thread.sleep(sleep);

            assertEquals(2, runningThreads);

            thread2.stop();
            Thread.sleep(sleep);

            assertEquals(1, runningThreads);

            thread1.stop();
            Thread.sleep(sleep);

            assertEquals(0, runningThreads);
        } catch(Exception e) {
            e.printStackTrace();
            fail(e.toString());
        }
    }

    @Test
    public void testPool() {
        try {
            Pool<Integer> pool = new Pool<>(Integer.class.getConstructor(int.class), -1);
            pool.add(new Integer(42));
            pool.add(new Integer(42));
            assertEquals(42, pool.get().intValue());
            assertEquals(42, pool.get().intValue());
            assertEquals(-1, pool.get().intValue());
            pool.add(new Integer(42));
            pool.clear();
            assertEquals(-1, pool.get().intValue());
        } catch(Exception e) {
            e.printStackTrace();
            fail(e.toString());
        }
    }

    @Test
    public void testTextures() {
        try {
            Textures.load();
            assertNotNull(Textures.imgBullet);
            assertNotNull(Textures.imgPlayerShip);
            assertNotNull(Textures.imgEnemyShip);
            assertNotNull(Textures.imgAsteroid);
            for(int i = 0; i < Textures.imgAsteroid.length; ++i)
                assertNotNull(Textures.imgAsteroid[i]);
        } catch(Exception e) {
            e.printStackTrace();
            fail(e.toString());
        }
    }
    
    @Test
    public void testEntities() {
        try {
            Entity entity1 = new Entity(null, 2f, 1f), entity2 = new Entity(null, 2f, 1f);
            entity1.setPosition(-1, -1);
            entity2.setPosition(+1, +1);
            assertTrue(entity1.isCollidingWith(entity2, 0f));
            assertTrue(entity2.isCollidingWith(entity1, 0f));
            entity1.setPosition(-4, -4);
            assertFalse(entity1.isCollidingWith(entity2, 0f));
            assertFalse(entity2.isCollidingWith(entity1, 0f));
        } catch(Exception e) {
            e.printStackTrace();
            fail(e.toString());
        }
    }
}
